-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 08, 2020 at 05:24 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.3.11-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moxy`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_timer`
--

CREATE TABLE `app_timer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_dt` time DEFAULT NULL,
  `to_dt` time DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_timer`
--

INSERT INTO `app_timer` (`id`, `from_dt`, `to_dt`, `status`, `created_at`, `updated_at`) VALUES
(1, '10:30:00', '13:45:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `artworks`
--

CREATE TABLE `artworks` (
  `c_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `featured` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0 for images, 1 for video',
  `show_art` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artworks`
--

INSERT INTO `artworks` (`c_id`, `image`, `status`, `user`, `created_at`, `updated_at`, `featured`, `type`, `show_art`) VALUES
(14, 'php9qo6g5.jpg', 1, 'first', NULL, NULL, 1, 0, 1),
(15, 'phpkBU0t1.jpg', 1, '2', NULL, NULL, 1, 0, 1),
(16, 'phpMCqfq8.jpg', 1, '3', NULL, NULL, 1, 0, 1),
(17, 'phpxUyNjf.jpg', 1, '4', NULL, NULL, 1, 0, 1),
(18, 'phpqrHlqr.jpg', 1, '5', NULL, NULL, 1, 0, 1),
(140, '1818693262.png', 0, '1818693262.png', '2019-12-10 11:26:06', '2019-12-10 11:26:06', 0, 0, 1),
(141, '71925216.png', 1, '71925216.png', '2019-12-10 13:35:26', '2019-12-10 13:35:26', 0, 0, 1),
(144, '1496557946.png', 1, '1496557946.png', '2019-12-17 15:14:49', '2019-12-17 15:14:49', 0, 0, 1),
(145, '1659380377.png', 0, '1659380377.png', '2019-12-17 15:14:51', '2019-12-17 15:14:51', 0, 0, 1),
(146, '1836445964.png', 0, '1836445964.png', '2019-12-17 15:19:10', '2019-12-17 15:19:10', 0, 0, 1),
(147, '709416849.png', 0, '709416849.png', '2019-12-17 15:42:29', '2019-12-17 15:42:29', 0, 0, 1),
(148, '1161684764.png', 1, '1161684764.png', '2019-12-17 15:45:20', '2019-12-17 15:45:20', 0, 0, 1),
(149, '613415663.png', 0, '613415663.png', '2019-12-18 15:29:07', '2019-12-18 15:29:07', 0, 0, 1),
(150, '1554533742.png', 0, '1554533742.png', '2019-12-18 15:33:56', '2019-12-18 15:33:56', 0, 0, 1),
(151, '899087001.png', 0, '899087001.png', '2019-12-18 15:49:26', '2019-12-18 15:49:26', 0, 0, 1),
(152, '959497230.png', 0, '959497230.png', '2019-12-18 16:45:12', '2019-12-18 16:45:12', 0, 0, 1),
(153, '713631180.png', 0, '713631180.png', '2019-12-18 16:50:02', '2019-12-18 16:50:02', 0, 0, 1),
(154, '267851507.png', 0, '267851507.png', '2019-12-18 16:51:54', '2019-12-18 16:51:54', 0, 0, 1),
(155, '999434314.png', 0, '999434314.png', '2019-12-18 16:55:04', '2019-12-18 16:55:04', 0, 0, 1),
(156, '365646973.png', 1, '365646973.png', '2019-12-18 18:10:41', '2019-12-18 18:10:41', 1, 0, 1),
(157, '406785147.png', 0, '406785147.png', '2019-12-18 18:25:58', '2019-12-18 18:25:58', 0, 0, 1),
(158, '119140170.png', 0, '119140170.png', '2019-12-18 18:52:26', '2019-12-18 18:52:26', 0, 0, 1),
(159, '584810372.png', 1, '584810372.png', '2019-12-19 07:57:46', '2019-12-19 07:57:46', 1, 0, 1),
(160, '1034308915.png', 0, '1034308915.png', '2019-12-19 09:45:25', '2019-12-19 09:45:25', 0, 0, 1),
(161, '147691306.png', 0, '147691306.png', '2019-12-19 10:02:42', '2019-12-19 10:02:42', 0, 0, 1),
(162, '1886024921.png', 0, '1886024921.png', '2019-12-19 10:58:20', '2019-12-19 10:58:20', 0, 0, 1),
(163, '1915690803.png', 0, '1915690803.png', '2019-12-19 14:45:27', '2019-12-19 14:45:27', 0, 0, 1),
(164, '75771855.png', 0, '75771855.png', '2019-12-19 15:02:35', '2019-12-19 15:02:35', 0, 0, 1),
(165, '1630491887.png', 0, '1630491887.png', '2019-12-19 15:51:49', '2019-12-19 15:51:49', 0, 0, 1),
(166, '79931267.png', 0, '79931267.png', '2019-12-19 16:16:34', '2019-12-19 16:16:34', 0, 0, 1),
(167, '1782699441.png', 0, '1782699441.png', '2019-12-19 16:19:22', '2019-12-19 16:19:22', 0, 0, 1),
(168, '551238423.png', 0, '551238423.png', '2019-12-19 16:21:48', '2019-12-19 16:21:48', 0, 0, 1),
(169, '646872911.png', 0, '646872911.png', '2019-12-20 04:48:17', '2019-12-20 04:48:17', 0, 0, 1),
(170, '1522805070.png', 0, '1522805070.png', '2019-12-20 04:48:43', '2019-12-20 04:48:43', 0, 0, 1),
(171, '1428124518.png', 0, '1428124518.png', '2019-12-20 04:56:33', '2019-12-20 04:56:33', 0, 0, 1),
(172, '1049710036.png', 0, '1049710036.png', '2019-12-20 07:36:03', '2019-12-20 07:36:03', 0, 0, 1),
(173, '785880022.png', 0, '785880022.png', '2019-12-20 12:37:27', '2019-12-20 12:37:27', 0, 0, 1),
(174, '1998183492.png', 0, '1998183492.png', '2019-12-20 14:56:16', '2019-12-20 14:56:16', 0, 0, 1),
(175, '574707467.png', 0, '574707467.png', '2019-12-20 16:13:10', '2019-12-20 16:13:10', 0, 0, 1),
(176, '2135831427.png', 0, '2135831427.png', '2019-12-20 16:13:11', '2019-12-20 16:13:11', 0, 0, 1),
(177, '326062092.png', 0, '326062092.png', '2019-12-20 16:16:34', '2019-12-20 16:16:34', 0, 0, 1),
(178, '616539907.png', 0, '616539907.png', '2019-12-20 17:11:30', '2019-12-20 17:11:30', 0, 0, 1),
(179, '672701582.png', 0, '672701582.png', '2019-12-20 19:45:08', '2019-12-20 19:45:08', 0, 0, 1),
(180, '1113234955.png', 0, '1113234955.png', '2019-12-23 09:47:07', '2019-12-23 09:47:07', 0, 0, 1),
(181, '1740051783.png', 0, '1740051783.png', '2019-12-23 09:48:00', '2019-12-23 09:48:00', 0, 0, 1),
(182, '488559192.png', 0, '488559192.png', '2019-12-23 09:49:35', '2019-12-23 09:49:35', 0, 0, 1),
(183, '1269683182.png', 0, '1269683182.png', '2019-12-23 09:50:52', '2019-12-23 09:50:52', 0, 0, 1),
(184, '714601588.png', 0, '714601588.png', '2019-12-23 09:54:51', '2019-12-23 09:54:51', 0, 0, 1),
(185, '627333713.png', 0, '627333713.png', '2019-12-23 10:35:00', '2019-12-23 10:35:00', 0, 0, 1),
(186, '160498756.png', 0, '160498756.png', '2020-01-03 17:35:03', '2020-01-03 17:35:03', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `canvas`
--

CREATE TABLE `canvas` (
  `c_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `canvas`
--

INSERT INTO `canvas` (`c_id`, `image`, `status`, `user`, `created_at`, `updated_at`) VALUES
(71, 'phpdvfn9a.png', '1', 'Black Spray', NULL, NULL),
(72, 'phpm1heu3.png', '1', 'Colourful background', NULL, NULL),
(73, 'php12J0cs.png', '1', 'Pink Grunge', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countrylist`
--

CREATE TABLE `countrylist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countrylist`
--

INSERT INTO `countrylist` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'United States of America', NULL, NULL),
(2, 'Albania', NULL, NULL),
(3, 'Algeria', NULL, NULL),
(4, 'American Samoa', NULL, NULL),
(5, 'Andorra', NULL, NULL),
(6, 'Angola', NULL, NULL),
(7, 'Anguilla', NULL, NULL),
(8, 'Antigua & Barbuda', NULL, NULL),
(9, 'Argentina', NULL, NULL),
(10, 'Armenia', NULL, NULL),
(11, 'Aruba', NULL, NULL),
(12, 'Australia', NULL, NULL),
(13, 'Austria', NULL, NULL),
(14, 'Azerbaijan', NULL, NULL),
(15, 'Bahamas', NULL, NULL),
(16, 'Bahrain', NULL, NULL),
(17, 'Bangladesh', NULL, NULL),
(18, 'Barbados', NULL, NULL),
(19, 'Belarus', NULL, NULL),
(20, 'Belgium', NULL, NULL),
(21, 'Belize', NULL, NULL),
(22, 'Benin', NULL, NULL),
(23, 'Bermuda', NULL, NULL),
(24, 'Bhutan', NULL, NULL),
(25, 'Bolivia', NULL, NULL),
(26, 'Bonaire', NULL, NULL),
(27, 'Bosnia & Herzegovina', NULL, NULL),
(28, 'Botswana', NULL, NULL),
(29, 'Brazil', NULL, NULL),
(30, 'British Indian Ocean Ter', NULL, NULL),
(31, 'Brunei', NULL, NULL),
(32, 'Bulgaria', NULL, NULL),
(33, 'Burkina Faso', NULL, NULL),
(34, 'Burundi', NULL, NULL),
(35, 'Cambodia', NULL, NULL),
(36, 'Cameroon', NULL, NULL),
(37, 'Canada', NULL, NULL),
(38, 'Canary Islands', NULL, NULL),
(39, 'Cape Verde', NULL, NULL),
(40, 'Cayman Islands', NULL, NULL),
(41, 'Central African Republic', NULL, NULL),
(42, 'Chad', NULL, NULL),
(43, 'Channel Islands', NULL, NULL),
(44, 'Chile', NULL, NULL),
(45, 'China', NULL, NULL),
(46, 'Christmas Island', NULL, NULL),
(47, 'Cocos Island', NULL, NULL),
(48, 'Colombia', NULL, NULL),
(49, 'Comoros', NULL, NULL),
(50, 'Congo', NULL, NULL),
(51, 'Cook Islands', NULL, NULL),
(52, 'Costa Rica', NULL, NULL),
(53, 'Cote DIvoire', NULL, NULL),
(54, 'Croatia', NULL, NULL),
(55, 'Cuba', NULL, NULL),
(56, 'Curacao', NULL, NULL),
(57, 'Cyprus', NULL, NULL),
(58, 'Czech Republic', NULL, NULL),
(59, 'Denmark', NULL, NULL),
(60, 'Djibouti', NULL, NULL),
(61, 'Dominica', NULL, NULL),
(62, 'Dominican Republic', NULL, NULL),
(63, 'East Timor', NULL, NULL),
(64, 'Ecuador', NULL, NULL),
(65, 'Egypt', NULL, NULL),
(66, 'El Salvador', NULL, NULL),
(67, 'Equatorial Guinea', NULL, NULL),
(68, 'Eritrea', NULL, NULL),
(69, 'Estonia', NULL, NULL),
(70, 'Ethiopia', NULL, NULL),
(71, 'Falkland Islands', NULL, NULL),
(72, 'Faroe Islands', NULL, NULL),
(73, 'Fiji', NULL, NULL),
(74, 'Finland', NULL, NULL),
(75, 'France', NULL, NULL),
(76, 'French Guiana', NULL, NULL),
(77, 'French Polynesia', NULL, NULL),
(78, 'French Southern Ter', NULL, NULL),
(79, 'Gabon', NULL, NULL),
(80, 'Gambia', NULL, NULL),
(81, 'Georgia', NULL, NULL),
(82, 'Germany', NULL, NULL),
(83, 'Ghana', NULL, NULL),
(84, 'Gibraltar', NULL, NULL),
(85, 'Great Britain', NULL, NULL),
(86, 'Greece', NULL, NULL),
(87, 'Greenland', NULL, NULL),
(88, 'Grenada', NULL, NULL),
(89, 'Guadeloupe', NULL, NULL),
(90, 'Guam', NULL, NULL),
(91, 'Guatemala', NULL, NULL),
(92, 'Guinea', NULL, NULL),
(93, 'Guyana', NULL, NULL),
(94, 'Haiti', NULL, NULL),
(95, 'Hawaii', NULL, NULL),
(96, 'Honduras', NULL, NULL),
(97, 'Hong Kong', NULL, NULL),
(98, 'Hungary', NULL, NULL),
(99, 'Iceland', NULL, NULL),
(100, 'Indonesia', NULL, NULL),
(101, 'India', NULL, NULL),
(102, 'Iran', NULL, NULL),
(103, 'Iraq', NULL, NULL),
(104, 'Ireland', NULL, NULL),
(105, 'Isle of Man', NULL, NULL),
(106, 'Israel', NULL, NULL),
(107, 'Italy', NULL, NULL),
(108, 'Jamaica', NULL, NULL),
(109, 'Japan', NULL, NULL),
(110, 'Jordan', NULL, NULL),
(111, 'Kazakhstan', NULL, NULL),
(112, 'Kenya', NULL, NULL),
(113, 'Kiribati', NULL, NULL),
(114, 'Korea North', NULL, NULL),
(115, 'Korea South', NULL, NULL),
(116, 'Kuwait', NULL, NULL),
(117, 'Kyrgyzstan', NULL, NULL),
(118, 'Laos', NULL, NULL),
(119, 'Latvia', NULL, NULL),
(120, 'Lebanon', NULL, NULL),
(121, 'Lesotho', NULL, NULL),
(122, 'Liberia', NULL, NULL),
(123, 'Libya', NULL, NULL),
(124, 'Liechtenstein', NULL, NULL),
(125, 'Lithuania', NULL, NULL),
(126, 'Luxembourg', NULL, NULL),
(127, 'Macau', NULL, NULL),
(128, 'Macedonia', NULL, NULL),
(129, 'Madagascar', NULL, NULL),
(130, 'Malaysia', NULL, NULL),
(131, 'Malawi', NULL, NULL),
(132, 'Maldives', NULL, NULL),
(133, 'Mali', NULL, NULL),
(134, 'Malta', NULL, NULL),
(135, 'Marshall Islands', NULL, NULL),
(136, 'Martinique', NULL, NULL),
(137, 'Mauritania', NULL, NULL),
(138, 'Mauritius', NULL, NULL),
(139, 'Mayotte', NULL, NULL),
(140, 'Mexico', NULL, NULL),
(141, 'Midway Islands', NULL, NULL),
(142, 'Moldova', NULL, NULL),
(143, 'Monaco', NULL, NULL),
(144, 'Mongolia', NULL, NULL),
(145, 'Montserrat', NULL, NULL),
(146, 'Morocco', NULL, NULL),
(147, 'Mozambique', NULL, NULL),
(148, 'Myanmar', NULL, NULL),
(149, 'Nambia', NULL, NULL),
(150, 'Nauru', NULL, NULL),
(151, 'Nepal', NULL, NULL),
(152, 'Netherland Antilles', NULL, NULL),
(153, 'Netherlands(Holland, Europe)', NULL, NULL),
(154, 'Nevis', NULL, NULL),
(155, 'New Caledonia', NULL, NULL),
(156, 'New Zealand', NULL, NULL),
(157, 'Nicaragua', NULL, NULL),
(158, 'Niger', NULL, NULL),
(159, 'Nigeria', NULL, NULL),
(160, 'Niue', NULL, NULL),
(161, 'Norfolk Island', NULL, NULL),
(162, 'Norway', NULL, NULL),
(163, 'Oman', NULL, NULL),
(164, 'Pakistan', NULL, NULL),
(165, 'Palau Island', NULL, NULL),
(166, 'Palestine', NULL, NULL),
(167, 'Panama', NULL, NULL),
(168, 'Papua New Guinea', NULL, NULL),
(169, 'Paraguay', NULL, NULL),
(170, 'Peru', NULL, NULL),
(171, 'Phillipines', NULL, NULL),
(172, 'Pitcairn Island', NULL, NULL),
(173, 'Poland', NULL, NULL),
(174, 'Portugal', NULL, NULL),
(175, 'Puerto Rico', NULL, NULL),
(176, 'Qatar', NULL, NULL),
(177, 'Republic of Montenegro', NULL, NULL),
(178, 'Republic of Serbia', NULL, NULL),
(179, 'Reunion', NULL, NULL),
(180, 'Romania', NULL, NULL),
(181, 'Russia', NULL, NULL),
(182, 'Rwanda', NULL, NULL),
(183, 'St Barthelemy', NULL, NULL),
(184, 'St Eustatius', NULL, NULL),
(185, 'St Helena', NULL, NULL),
(186, 'St Kitts-Nevis', NULL, NULL),
(187, 'St Lucia', NULL, NULL),
(188, 'St Maarten', NULL, NULL),
(189, 'St Pierre & Miquelon', NULL, NULL),
(190, 'St Vincent & Grenadines', NULL, NULL),
(191, 'Saipan', NULL, NULL),
(192, 'Samoa', NULL, NULL),
(193, 'Samoa American', NULL, NULL),
(194, 'San Marino', NULL, NULL),
(195, 'Sao Tome & Principe', NULL, NULL),
(196, 'Saudi Arabia', NULL, NULL),
(197, 'Senegal', NULL, NULL),
(198, 'Seychelles', NULL, NULL),
(199, 'Sierra Leone', NULL, NULL),
(200, 'Singapore', NULL, NULL),
(201, 'Slovakia', NULL, NULL),
(202, 'Slovenia', NULL, NULL),
(203, 'Solomon Islands', NULL, NULL),
(204, 'Somalia', NULL, NULL),
(205, 'South Africa', NULL, NULL),
(206, 'Spain', NULL, NULL),
(207, 'Sri Lanka', NULL, NULL),
(208, 'Sudan', NULL, NULL),
(209, 'Suriname', NULL, NULL),
(210, 'Swaziland', NULL, NULL),
(211, 'Sweden', NULL, NULL),
(212, 'Switzerland', NULL, NULL),
(213, 'Syria', NULL, NULL),
(214, 'Tahiti', NULL, NULL),
(215, 'Taiwan', NULL, NULL),
(216, 'Tajikistan', NULL, NULL),
(217, 'Tanzania', NULL, NULL),
(218, 'Thailand', NULL, NULL),
(219, 'Togo', NULL, NULL),
(220, 'Tokelau', NULL, NULL),
(221, 'Tonga', NULL, NULL),
(222, 'Trinidad & Tobago', NULL, NULL),
(223, 'Tunisia', NULL, NULL),
(224, 'Turkey', NULL, NULL),
(225, 'Turkmenistan', NULL, NULL),
(226, 'Turks & Caicos Is', NULL, NULL),
(227, 'Tuvalu', NULL, NULL),
(228, 'Uganda', NULL, NULL),
(229, 'United Kingdom', NULL, NULL),
(230, 'Ukraine', NULL, NULL),
(231, 'United Arab Emirates', NULL, NULL),
(232, 'Afghanistan', NULL, NULL),
(233, 'Uruguay', NULL, NULL),
(234, 'Uzbekistan', NULL, NULL),
(235, 'Vanuatu', NULL, NULL),
(236, 'Vatican City State', NULL, NULL),
(237, 'Venezuela', NULL, NULL),
(238, 'Vietnam', NULL, NULL),
(239, 'Virgin Islands (Brit)', NULL, NULL),
(240, 'Virgin Islands (USA)', NULL, NULL),
(241, 'Wake Island', NULL, NULL),
(242, 'Wallis & Futana Is', NULL, NULL),
(243, 'Yemen', NULL, NULL),
(244, 'Zaire', NULL, NULL),
(245, 'Zambia', NULL, NULL),
(246, 'Zimbabwe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frontenduser`
--

CREATE TABLE `frontenduser` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `frontenduser`
--

INSERT INTO `frontenduser` (`id`, `first_name`, `last_name`, `country`, `email`, `created_at`, `updated_at`, `title`) VALUES
(1, 'testuser', 'test', 'NZ', 'testuser@gmail.com', '2019-11-19 06:14:09', '2019-11-19 06:14:09', 'ms');

-- --------------------------------------------------------

--
-- Table structure for table `frontusers`
--

CREATE TABLE `frontusers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `frontusers`
--

INSERT INTO `frontusers` (`id`, `title`, `first_name`, `last_name`, `country`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Mr', 'David', 'Clark', 'Afghanistan', 'david@yahoo.com', '2019-11-19 06:23:26', '2019-11-19 06:23:26'),
(3, 'Mrs', 'Lily', 'kewin', 'Colombia', 'lily@gmail.com', '2019-11-19 07:06:40', '2019-11-19 07:06:40'),
(5, 'Mrs', 'robert1', 'peterson', 'Australia', 'robert1@gmail.com', '2019-11-19 10:01:45', '2019-11-19 10:01:45'),
(6, 'Ms', 'Rosy', 'peterson', 'Belarus', 'rosy11@gmail.com', '2019-11-19 10:01:48', '2019-11-19 10:01:48'),
(7, 'Mr', 'test', 'oioioi', 'United States of America', 'rosy12@gmail.com', '2019-11-19 10:01:51', '2019-11-19 10:01:51'),
(8, 'Mrs', 'Jassica', 'micheelle', 'French Guiana', 'jassica@gmail.com', '2019-11-19 10:01:53', '2019-11-19 10:01:53'),
(10, 'Mrs', 'Rosy', 'peterson', 'India', 'rosy111111@gmail.com', '2019-11-19 10:01:57', '2019-11-19 10:01:57'),
(15, 'Mr', 'james', 'peter', 'Zimbabwe', 'james@gmail.com', '2019-11-20 09:58:36', '2019-11-20 09:58:36'),
(16, 'Mr', 'Test', 'Thanks', 'India', 'aa1@test.com', '2019-11-27 11:42:36', '2019-11-27 11:42:36'),
(17, 'Mr', 'Test', 'Thanks', 'India', 'aaa@test.com', '2019-11-27 11:47:42', '2019-11-27 11:47:42'),
(19, 'Mr', 'Rahul', 'Malhotra', 'Maldives', 'rahul.malhotra@gmail.com', '2019-12-02 15:50:15', '2019-12-02 15:50:15'),
(20, 'Ms', 'Susan', 'Kroll', 'United States of America', 'skroll@rareculture.com', '2020-01-03 17:34:58', '2020-01-03 17:34:58');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_01_064513_create_canvas_table', 1),
(5, '2019_11_04_072603_create_image_gallery_table', 2),
(6, '2019_11_04_093715_create_images_table', 3),
(7, '2019_11_06_060844_create_stop_app_table', 4),
(8, '2019_11_07_061641_create_sticker_table', 5),
(9, '2019_11_07_062242_create_sticker_cat_table', 6),
(10, '2019_11_07_065417_rename_stop_app_table', 7),
(11, '2019_11_11_102038_add_featur_to_artworks', 8),
(12, '2019_11_19_052501_create_frontenduser_table', 9),
(13, '2019_11_19_060133_add_title_to_frontenduser', 10),
(14, '2019_11_19_061957_create_frontusers_table', 11),
(15, '2019_11_27_075602_create_countrylist_table', 12),
(16, '2019_11_28_061240_create_tutorial_video_table', 13),
(17, '2019_11_28_070933_add_link_to_tutorial_video', 14),
(18, '2019_11_28_115503_add_show_art_to_artworks', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sticker`
--

CREATE TABLE `sticker` (
  `st_id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sticker`
--

INSERT INTO `sticker` (`st_id`, `cat_id`, `cat_name`, `image`, `name`, `status`, `created_at`, `updated_at`) VALUES
(26, 24, 'testing123', 'phpkxv1TC.jpg', 'fdfdfd', '1', NULL, NULL),
(31, 20, 'Hashtags', 'phpOeUyma.png', 'Hashtag_Stickerhashtag', '1', NULL, NULL),
(32, 10, 'Accessories', 'php0tR3Df.png', 'TabletSticker_Beanie', '1', NULL, NULL),
(33, 9, 'Hair', 'phpmya4Yq.png', 'TabletSticker_Blonde-Hair', '1', NULL, NULL),
(34, 18, 'Beard', 'phpuSmckl.png', 'TabletSticker_Brown-Beard', '1', NULL, NULL),
(35, 9, 'Hair', 'phpgAFAEI.png', 'TabletSticker_Mohawk', '1', NULL, NULL),
(36, 10, 'Accessories', 'phpOeBY0I.png', 'TabletSticker_Moxy-Hat', '1', NULL, NULL),
(37, 18, 'Beard', 'php0flvch.png', 'TabletSticker_Mustache', '1', NULL, NULL),
(38, 9, 'Hair', 'phpn97DyZ.png', 'TabletSticker_Pink-Wig', '1', NULL, NULL),
(39, 10, 'Accessories', 'php5OQFy7.png', 'TabletSticker_Rose-Tattoo', '1', NULL, NULL),
(40, 18, 'Beard', 'phpImp4Pt.png', 'TabletSticker_Salt-n-Pepper-Beard', '1', NULL, NULL),
(41, 10, 'Accessories', 'phpO5XYzw.png', 'TabletSticker_Sunglasses', '1', NULL, NULL),
(42, 10, 'Accessories', 'php5GkRur.png', 'TabletSticker_Tiara', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sticker_cat`
--

CREATE TABLE `sticker_cat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sticker_cat`
--

INSERT INTO `sticker_cat` (`id`, `category_name`, `status`, `created_at`, `updated_at`) VALUES
(8, 'Tags', 0, '2019-11-07 07:21:32', '2019-11-07 07:21:32'),
(9, 'Hair', 1, '2019-11-07 07:21:37', '2019-11-07 07:21:37'),
(10, 'Accessories', 1, '2019-11-07 07:21:44', '2019-11-07 07:21:44'),
(18, 'Beard', 1, '2019-11-08 15:34:19', '2019-11-08 15:34:19'),
(20, 'Hashtags', 1, '2019-11-08 20:39:18', '2019-11-08 20:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `tutorial_video`
--

CREATE TABLE `tutorial_video` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tutorial_video`
--

INSERT INTO `tutorial_video` (`id`, `name`, `status`, `created_at`, `updated_at`, `link`, `thumbnail`) VALUES
(4, 'phpP8b2mA.mp4', '1', NULL, NULL, 'http://54.71.80.232/Moxy/storage/app/public/phpP8b2mA.mp4', 'http://54.71.80.232/Moxy/storage/app/public/phpP8b2mA.mp4.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@moxy.com', NULL, '$2y$10$gwY9vh1oWDUJBE4WTuVDQe17Q73YCJnMzUAEaVlXq/Qt8G4uTsJre', 'c74GNvchQ0aI3pgV0AzwtyLtX6EX77D0HVyO9yMsEE4aapOLu7BgYnBMYszu', '2019-11-04 06:18:21', '2019-11-04 06:18:21'),
(2, 'jagpreet', 'jagpreet.kaur@qualhon.com', NULL, '$2y$10$sOaqSLpL4Gp4TooNO3c3sOIx1ppFccRtM2XPFPUX/MSW30DF8uTQa', NULL, '2019-11-04 07:02:32', '2019-11-04 07:02:32'),
(3, 'amit', 'amit@gmail.com', NULL, '$2y$10$9ToQJcFiRYf/rOD36eLQcepjTLLYyKSA5LZvWR5aZjk17x2FBVBHS', '7rbvesKNJ1OcLSTo9YtzuiqMXvxrQtkVNQpQKJtrTCet0j6262cGplb4Ua0g', '2019-11-15 07:40:53', '2019-11-15 07:40:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_timer`
--
ALTER TABLE `app_timer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artworks`
--
ALTER TABLE `artworks`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `canvas`
--
ALTER TABLE `canvas`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `countrylist`
--
ALTER TABLE `countrylist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontenduser`
--
ALTER TABLE `frontenduser`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frontenduser_email_unique` (`email`);

--
-- Indexes for table `frontusers`
--
ALTER TABLE `frontusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sticker`
--
ALTER TABLE `sticker`
  ADD PRIMARY KEY (`st_id`);

--
-- Indexes for table `sticker_cat`
--
ALTER TABLE `sticker_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tutorial_video`
--
ALTER TABLE `tutorial_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_timer`
--
ALTER TABLE `app_timer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `artworks`
--
ALTER TABLE `artworks`
  MODIFY `c_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT for table `canvas`
--
ALTER TABLE `canvas`
  MODIFY `c_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `countrylist`
--
ALTER TABLE `countrylist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frontenduser`
--
ALTER TABLE `frontenduser`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `frontusers`
--
ALTER TABLE `frontusers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sticker`
--
ALTER TABLE `sticker`
  MODIFY `st_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `sticker_cat`
--
ALTER TABLE `sticker_cat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tutorial_video`
--
ALTER TABLE `tutorial_video`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
