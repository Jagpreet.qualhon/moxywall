<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use App\StickerCat;
use App\Canvas;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Hash;

use View;

class AdminController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //die('in controller');
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        //
    }
    public function canvas_list()
    {
        $canva=DB::table('canvas')->get();
        return view('canvas.canvas_list')->with(['canvas_list'=>$canva]);
    }

    /** Add image view in canvas: */


    public function Add_image()
    {

        return view('canvas.Add_image');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function change_status(Request $request){

         if($request->status==1)
         {
             $s=0;
         }else
         {
            $s=1;
         }
         DB::table('canvas')->where('c_id',$request->id)->update(['status'=>$s]);
    
             return $s;

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add_canvas_image(Request $request)
    {
        print_r($request->myfile);

        $this->validate($request,[
            'image' => 'mimes:jpeg,jpg,png,gif|required',

            ]);

        $cover = $request->file('image');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));
        // print_r($cover->getFilename().'.'.$extension);

        // echo $request->image_name;
        // exit;
         $img=$cover->getFilename().'.'.$extension;


         DB::table('canvas')->insert(['image'=>$img,'user'=>$request->image_name,'status'=>$request->status]);

        return back()->with('success','Canvas has been added successfully !! ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy1($id)
    {
        //
    }

    public function gallery()
    {

        $canva=DB::table('canvas')->get();
        //print_r($canva);die;
       return view('canvas.gallery')->with(['canvas_list'=>$canva]);
    }

    public function delete_canvas(Request $request) {

        DB::table('canvas')->where('c_id',$request->id)->delete();
        return redirect('gallery');


       }

    public function timescreen()  {

    $timedata =DB::table('app_timer')->get();

    return view('time.timescreen')->with(['timedata' => $timedata]);

    }

    public function add_time(Request $request)  {

        $this->validate($request,[
            'from_dt' =>'required',
            'to_dt' => 'required'
   
   
            ]);

        $timedata =DB::table('app_timer')->get();

        if(count($timedata) == 0){

            DB::table('app_timer')->insert(
                [
                    'from_dt'=>$request->from_dt,
                    'to_dt'=>$request->to_dt,
                    'status'=>1
                ]
            );
            return back()->with('success','Your App datetime has been added successfully !! ');

        }else{

            DB::table('app_timer')->update(
                [
                    'from_dt'=>$request->from_dt,
                    'to_dt'=>$request->to_dt,
                    'status'=>1
                ]
            );


        }
            return back()->with('success','Your App datetime has been changed successfully !! ');


        }

        public function clear_timer(Request $request)  {

            DB::table('app_timer')->where('id',1)->update(
                    [
                        'from_dt'=>NULL,
                        'to_dt'=>NULL,
                        'status'=>0
                    ]
                );

              return redirect('timescreen');


}

    public function add_cat()
    {

        return view('stickers.add_cat');
    }

    public function addcat(Request $request)
    {

        $this->validate($request,[
            'cat_name' => 'required|unique:sticker_cat,category_name',

            ]);

        if(DB::table('sticker_cat')->insert(
            [
            'category_name'=>$request->cat_name

            ]
            )){
        return back()->with('success','A new Category has been addded successfully !! ');
            }else{
                return back()->with('error','Some Problem has been occurred while adding new category !! ');

            }
    }


    public function managecategory()
    {
        // $cat = DB::table('sticker_cat')
        // ->select('*')
        // ->where('id',$id)
        // ->get();

        $category=DB::table('sticker_cat')->get();
        return view('stickers.managecategory')->with(['category'=>$category]);
    }

    public function edit_cat($id) {
     
        $cat = DB::table('sticker_cat')
          ->select('*')
          ->where('id',$id)
          ->get();
        
         return view('templates.edit_donor',['donor'=>$donor]);
                  
         }



    public function change_status_cat(Request $request){

        if($request->status==1)
        {
            $s=0;
        }else
        {
           $s=1;
        }
        DB::table('sticker_cat')->where('id',$request->id)->update(['status'=>$s]);

       return $s;

    }


    public function delete_category(Request $request) {

        DB::table('sticker_cat')->where('id',$request->id)->delete();
        return redirect('managecategory');


       }


       public function update_cat(Request $request, $id){

        $this->validate($request,[
            'cat_name' => 'required',

            ]);

            
        $cat = DB::table('sticker_cat')
        ->where('id',$id)
        ->update([
           'category_name'=> $request->input('cat_name')
            
                ]);

        if($cat){

            DB::table('sticker')
        ->where('cat_id',$id)
        ->update([
           'cat_name'=> $request->input('cat_name')
            
                ]);

        }
     
    return back()->with('success','Category Name has been updated successfully  !! ');


       }

       public function filter_cat(Request $request){

       // echo $request->cat; die;
       if($request->cat =='all'){
            
        return redirect('/stickers');
        } 
       
       $cat = StickerCat::all();
       $stickers = DB::table('sticker')
        ->select('*')
        ->where('cat_id', $request->cat)
        ->get();

        $data=[];
        $cate =DB::table('sticker_cat')->select('*')->where('id',$request->cat)->get();

        $data[$cate[0]->category_name]=DB::table('sticker')->where('cat_id',$request->cat)->get();
        //echo "<pre>";print_r($data);die;

        return view('stickers.stickers')->with(['stickers' => $stickers,'cat' => $cat,'data'=>$data]);
     }


     public function filter_status(Request $request)

       {

           if($request->status =='0'){

            $canva = DB::table('canvas')
            ->select('*')
            ->where('status', $request->status)
            ->get();



           }else if($request->status =='1'){

            $canva = DB::table('canvas')
            ->select('*')
            ->where('status', $request->status)
            ->get();

           }else{

            $canva=DB::table('canvas')->get();

           }

           return view('canvas.gallery')->with(['canvas_list'=>$canva]);



    }

    public function add_sticker()
    {
        $cat =StickerCat::where('status',1)->get();
        return view('stickers.add_sticker')->with(['cat' => $cat]);
    }



    public function addsticker(Request $request)
    {
        print_r($request->myfile);

        $this->validate($request,[
            'image' => 'mimes:jpeg,jpg,png,gif|required',

            ]);

        $cover = $request->file('image');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));
        
         $img=$cover->getFilename().'.'.$extension;

         $stt = DB::table('sticker_cat')
                ->where('id', '=', $request->cat)
                ->first();


         DB::table('sticker')->insert(
             [
        'cat_id'=>$request->cat,
        'image'=>$img,
        'name'=>$request->image_name,
        'cat_name'=>$stt->category_name,
        'status'=>$request->status
        ]
        );
        return back()->with('success','A new sticker has been addded successfully  !! ');
    }


    public function stickers()
    {

    $stickers =DB::table('sticker')->get();
    $cat =StickerCat::all();
    
    $data=[];
    foreach($cat as $c){

        $data[$c->category_name]=DB::table('sticker')->where('cat_id',$c->id)->get();
       
       

    }
  
    return view('stickers.stickers')->with(['stickers' => $stickers, 'cat' => $cat,'data'=>$data]);

    }


    public function change_status_stk(Request $request){

        if($request->status==1)
        {
            $s=0;
        }else
        {
           $s=1;
        }
        DB::table('sticker')->where('st_id',$request->id)->update(['status'=>$s]);

       return $s;

    }

    public function delete_sticker(Request $request) {

        DB::table('sticker')->where('st_id',$request->id)->delete();
        return redirect('stickers');


       }


    public function timer()
    {

    $timedata =DB::table('app_timer')->get();

    return view('time.timer')->with(['timedata' => $timedata]);

    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("canvas")->whereIn('c_id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Canvas Deleted successfully."]);
    }


    public function destroy(Request $request,$id){

        $canvas=Canvas::find($id);

        $canvas->delete();

        return back()->with('success','Canvas deleted successfully');

    }


    /** Art work Gallery : Start*/

    /** Art work Gallery : End */


    /** Change Password Admin : Start*/

    
    public function change_pwd(){

        $current_pwd = DB::table('users')
                        ->select('*')
                        ->where('email', Auth::user()->email)
                        ->get();

        return view('admin.change_pwd')->with(['current_pwd'=> $current_pwd ]);

    }

    public function update_pwd(Request $request){
        
        $this->validate($request, [
            'current_password'=> 'required',
            'password' => 'required|min:8|max:12',
            'confirm_password' => 'required|same:password'
        ]); 
        
        $user = Auth::user();
        
        if(!\Hash::check($request->current_password, $user->password)) {
            return back()->with('error','You have entered incorrect current password . Try Again !!');
        } else {
            $admin = DB::table('users')
            ->where('email',$user->email)
            ->update([          
                    'password'=> Hash::make($request->input('password')),
               ]);
               return back()->with('success','Your password has been changed successfully !!');
       
            }

    }


    /** Change Password Admin : End */


    /** Upload Tutorial Video : Start */


    public function add_video()
    {

   // $timedata =DB::table('app_timer')->get();

    return view('video.video');

    }

    
    public function upload_video(Request $request)
    {
        print_r($request->myfile);

        $this->validate($request,[
            'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required',

            ]);

        
    $vd =DB::table('tutorial_video')->get();

    if(count($vd) == 0){

        $cover = $request->file('video');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));
        $img=$cover->getFilename().'.'.$extension;
        $link = asset('storage/app/public/'.$img);
        $videoThumbnail =  $this->getVideoThumbnail(storage_path('app/public/'.$img),$img);
        
        DB::table('tutorial_video')->insert(['name'=>$img, 'link'=>$link, 'thumbnail'=>$videoThumbnail]);
        return back()->with('success','Tutorial video has been uploaded successfully !! ');

    }else{

        $cover = $request->file('video');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));  
        $img=$cover->getFilename().'.'.$extension;
        $link = asset('storage/app/public/'.$img);
        $videoThumbnail =  $this->getVideoThumbnail(storage_path('app/public/'.$img),$img);

        DB::table('tutorial_video')->update(['name'=>$img, 'link'=>$link, 'thumbnail'=>$videoThumbnail]);
        return back()->with('success','Tutorial video has been changed successfully !! ');

    }

    
}

    protected function getVideoThumbnail($location, $image_name) {
        
        exec('ffmpeg -i '.$location.' -vf  "thumbnail,scale=640:360" -frames:v 1 '.storage_path('app/public/'.$image_name).'.png');
        return asset('storage/app/public/'.$image_name.'.png');
    }



    /** Upload tutorial Video : End */

}
