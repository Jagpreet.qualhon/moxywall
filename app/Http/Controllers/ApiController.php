<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Response;
use Validator;
use Auth;
use Mail;
use App\FrontEndUser;
use App\Artwork;
use App\StickerCat;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */


    public function response($response,$status='1') {  
        header("Content-Type: application/json");       
        $out = array();
        $out['status'] = $status;
        $out['response'] = $response;
        if(empty($response)) {
            $out['status'] = '0';
            $out['response'] = '';
        }        
        echo json_encode($out);
    }



    public function index()
    {
        $canva=DB::table('canvas')->where('status',1)->get();
        return $this->response($canva);
    }


    public function loop_reel(Request $request)
    {
        $loop_reel=DB::table('artworks')->where('featured', '1')->orderby('c_id','desc')->get();
        $vdThumb = DB::table('tutorial_video')->select('thumbnail')->get();
        $vd =  array (
            'image' => $vdThumb,
            'type'  => '1'
          
        );
        $arr=json_decode(json_encode($loop_reel), true); 
        array_push($arr,$vd);

        return $this->response($arr);
    }

    public function stickers(){
    
    $category =StickerCat::where('status',1)->get();
    $response=[];
    
    foreach($category as $c){

        $sticker=DB::table('sticker')
                    ->where('cat_id',$c->id)
                    ->where('status',1)
                    ->get();
        $ct=['id'=>$c->id ,
            'cat_name'=>$c->category_name,
            $c->category_name=>$sticker];
        $response['category'][]=  $ct;
    }
        return $this->response($response);
    }
    


    public function user(Request $request){

        $rules=[
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'email' => 'required|email|unique:frontusers'
            ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
           return array('status'=>'0', 'message'=>response()->json($validator->errors(), 422));
        }
    
        $user = new FrontEndUser;
        $user->title = $request->title;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->country = $request->country;
        $user->email = $request->email;
       
        $user->save();
        return $this->response('New user has been created');
    }

    public function country(){

        $country=DB::table('countrylist')->select('id','name')->get();
        return $this->response($country);

    }

    public function tutorial_video(){

        $vd=DB::table('tutorial_video')->select('link')->first();
        return $this->response($vd);


    }
    protected function br2nl($string) {
        return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);

    }

    public function testmail() {

       echo env("MAILGUN_KEY");
      // die('in die');
        $to = 'jagpreet.kaur@qualhon.com';
        $message = "<b>Dummy132</b>";
        $subject = "Dummy132";
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:'.env('MAILGUN_KEY'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $plain = strip_tags($this->br2nl($message));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.env('MAILGUN_DOMAIN').'/messages');
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'from' => 'no-reply@qualhon.com',
            'to' => $to,
            'subject' => $subject,
            'html' => $message,
            'text' => $plain
            )
        );
        $j = json_decode(curl_exec($ch));
        $info = curl_getinfo($ch);
        if($info['http_code'] != 200)
            
        curl_close($ch);         
    
    }
    

    public function new_artwork(Request $request){

         $rules=[
            'show_art' => 'required',
            'image' =>'required'
            ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
           return array('status'=>'0', 'message'=>response()->json($validator->errors(), 422));
        }
        if($request->show_art == 1){

            $image = $request->image;  
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = rand().'.'.'png';
            Storage::disk('public')->put($imageName, base64_decode($image));
            $url = 'https://cutt.ly/api/api.php';
            $key= '9f2d2c686fbb4d1b447edb70ac513bc26ac5f';         
            $link = asset('storage/app/public/'.$imageName);
            $name = 'Artwork';
                 
        //     $json = file_get_contents($url."?key=$key&short=$link&name=$name");
        //     echo $url."?key=$key&short=$link&name=$name";
        //     $data = json_decode($json, true);
        //     print_r($data); die;
            $art = new Artwork;
            $art->image = $imageName;
            $art->status = 0;
            $art->featured = 0;
            $art->type = 0;
            $art->user = $imageName;
            $art->show_art = $request->show_art;
        
            $art->save();
           
            if($request->phone){

                $to_phone = $request->phone;
                $str = $to_phone;
                $str = str_replace(array(' ','-'), array('',''), $str);
                $to_phone = $str;
                if(substr($to_phone, 0, 1)=='+') {
                    $to_phone = ltrim($str, '+');
                } else {
                    if(substr($str, 0, 2)!='91') {
                        $to_phone = '91'.$str;
                    }
                }
    
                $message = "Graffiti Wall App - Click the link to get your artwork .".$link;
    
                $url = 'https://rest.nexmo.com/sms/json?api_key=a9a261c4&api_secret=ISSLlw6F3DRljXqA&from=Moxy&to='.$to_phone.'&text='.urlencode($message);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
            }

            if($request->email){

            $baseurl = url('/');
            $to = $request->email;
            $message = "<b>Artwork</b>";
            $subject = "Get Your Artwork";
            $html = '<!doctype html>
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <!--<![endif]-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
            <title></title>
            <style>
            .wrapper {
                width: 100%;
                table-layout: fixed;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
            }
            </style>
            </head>
            
            <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
                <center class="wrapper" style="width:100%;table-layout:fixed;
                -webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fff;">
                <table style="border: 10px solid #000;background: #fff;margin: 50px 0;padding: 40px 54px 24px;width: 700px;">
                <tbody>
                    <tr>
                        <td>
                          <table style="width: 166px;margin:0 auto;padding-bottom: 16px;">
                              <tbody>
                                  <tr>
                                      <td>
                                            <img src="'.$baseurl.'/dist/Moxy_East_Village_Logo.png" style="max-width:100%">
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 style="font-family: Open Sans, sans-serif;text-align: center;color: #aa0b79;font-weight: 400;font-size: 34px;
                            letter-spacing: 1px;">HI THERE!</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: Open Sans, sans-serif;margin: 20px 0 18px;font-size: 16px;"><strong>Dear Guest,</strong></p>
                            <p style="font-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin-bottom: 11px;">Thank you for visiting us at Moxy NYC East Village! Please use the link below to
                                 access your artwork and enjoy
                                sharing it with your friends and family.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="ffont-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin: 0;">Artwork link: <a href="'.$link.'" style="color: #075dfc;font-weight: 600;">'.$link.'</a></p>
                        </td>
                    </tr>
                    <tr>
                        <td><p style="font-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin: 11px 0 0;">We hope to see you again soon.</p></td>
                    </tr>
                    <tr>
                        <td>
                                <table style="margin:0 auto;margin-top: 100px;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                      <p style="font-family: Open Sans, sans-serif;font-size: 13px;margin:0;">Please do not reply to this message as we will be not able to receive your request</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </center>
            </body>
            </html>';
            $ch = curl_init();
     
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:key-487bee64527dee24be89ff9f5dcaf856');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $plain = strip_tags($this->br2nl($message));

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/qualhon.org/messages');
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                'from' => 'no-reply@qualhon.com',
                'to' => $to,
                'subject' => $subject,
                'html' => $html,
                'text' => $plain
                )
            );
            $j = json_decode(curl_exec($ch));
            $info = curl_getinfo($ch);
            if($info['http_code'] != 200)
            curl_close($ch);  
                
        }
        return $this->response('New art work has been uploaded and link for the image has been sent!!');
            }else{

            $image = $request->image;  
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = rand().'.'.'png';
            Storage::disk('public')->put($imageName, base64_decode($image));
            $link = asset('storage/app/public/'.$imageName);
            

            
       $baseurl = url('/');
            $to = $request->email;
            $message = "<b>Artwork</b>";
            $subject = "Get Your Artwork";
            $html = '<!doctype html>
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <!--<![endif]-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
            <title></title>
            <style>
            .wrapper {
                width: 100%;
                table-layout: fixed;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
            }
            </style>
            </head>
            
            <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
                <center class="wrapper" style="width:100%;table-layout:fixed;
                -webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fff;">
                <table style="border: 10px solid #000;background: #fff;margin: 50px 0;padding: 40px 54px 24px;width: 700px;">
                <tbody>
                    <tr>
                        <td>
                          <table style="width: 166px;margin:0 auto;padding-bottom: 16px;">
                              <tbody>
                                  <tr>
                                      <td>
                                            <img src="'.$baseurl.'/dist/Moxy_East_Village_Logo.png" style="max-width:100%">
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 style="font-family: Open Sans, sans-serif;text-align: center;color: #aa0b79;font-weight: 400;font-size: 34px;
                            letter-spacing: 1px;">HI THERE!</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: Open Sans, sans-serif;margin: 20px 0 18px;font-size: 16px;"><strong>Dear Guest,</strong></p>
                            <p style="font-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin-bottom: 11px;">Thank you for visiting us at Moxy NYC East Village! Please use the link below to
                                 access your artwork and enjoy
                                sharing it with your friends and family.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="ffont-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin: 0;">Artwork link: <a href="'.$link.'" style="color: #075dfc;font-weight: 600;">'.$link.'</a></p>
                        </td>
                    </tr>
                    <tr>
                        <td><p style="font-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin: 11px 0 0;">We hope to see you again soon.</p></td>
                    </tr>
                    <tr>
                        <td>
                                <table style="margin:0 auto;margin-top: 100px;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                      <p style="font-family: Open Sans, sans-serif;font-size: 13px;margin:0;">Please do not reply to this message as we will be not able to receive your request</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </center>
            </body>
            </html>';
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:key-487bee64527dee24be89ff9f5dcaf856');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $plain = strip_tags($this->br2nl($message));

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/qualhon.org/messages');
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                'from' => 'no-reply@qualhon.com',
                'to' => $to,
                'subject' => $subject,
                'html' => $html,
                'text' => $plain
                )
            );
            $j = json_decode(curl_exec($ch));
            $info = curl_getinfo($ch);
            if($info['http_code'] != 200)
                
            curl_close($ch);  

            if($request->phone){

                $to_phone = $request->phone;
                $str = $to_phone;
                $str = str_replace(array(' ','-'), array('',''), $str);
                $to_phone = $str;
                if(substr($to_phone, 0, 1)=='+') {
                    $to_phone = ltrim($str, '+');
                } else {
                    if(substr($str, 0, 2)!='91') {
                        $to_phone = '91'.$str;
                    }
                }

                $message = "Graffiti Wall App - Click the link to get your artwork .".$link;
                $url = 'https://rest.nexmo.com/sms/json?api_key=a9a261c4&api_secret=ISSLlw6F3DRljXqA&from=Moxy&to='.
                    $to_phone.'&text='.urlencode($message);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);

            }
    
            if($request->email){
                
                $baseurl = url('/');
                $to = $request->email;
                $message = "<b>Artwork</b>";
                $subject = "Get Your Artwork";
                $html = '<!doctype html>
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <!--[if !mso]><!-->
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <!--<![endif]-->
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
                <title></title>
                <style>
                .wrapper {
                    width: 100%;
                    table-layout: fixed;
                    -webkit-text-size-adjust: 100%;
                    -ms-text-size-adjust: 100%;
                }
                </style>
                </head>
                
                <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
                    <center class="wrapper" style="width:100%;table-layout:fixed;
                    -webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fff;">
                    <table style="border: 10px solid #000;background: #fff;margin: 50px 0;padding: 40px 54px 24px;width: 700px;">
                    <tbody>
                        <tr>
                            <td>
                              <table style="width: 166px;margin:0 auto;padding-bottom: 16px;">
                                  <tbody>
                                      <tr>
                                          <td>
                                                <img src="'.$baseurl.'/dist/Moxy_East_Village_Logo.png" style="max-width:100%">
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2 style="font-family: Open Sans, sans-serif;text-align: center;color: #aa0b79;font-weight: 400;font-size: 34px;
                                letter-spacing: 1px;">HI THERE!</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="font-family: Open Sans, sans-serif;margin: 20px 0 18px;font-size: 16px;"><strong>Dear Guest,</strong></p>
                                <p style="font-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin-bottom: 11px;">Thank you for visiting us at Moxy NYC East Village! Please use the link below to
                                     access your artwork and enjoy
                                    sharing it with your friends and family.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="ffont-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin: 0;">Artwork link: <a href="'.$link.'" style="color: #075dfc;font-weight: 600;">'.$link.'</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td><p style="font-family: Open Sans, sans-serif;font-size: 15px;line-height: 28px;margin: 11px 0 0;">We hope to see you again soon.</p></td>
                        </tr>
                        <tr>
                            <td>
                                    <table style="margin:0 auto;margin-top: 100px;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                          <p style="font-family: Open Sans, sans-serif;font-size: 13px;margin:0;">Please do not reply to this message as we will be not able to receive your request</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </center>
                </body>
                </html>';
                $ch = curl_init();
    
                
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, 'api:key-487bee64527dee24be89ff9f5dcaf856');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $plain = strip_tags($this->br2nl($message));
    
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/qualhon.org/messages');
                curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                    'from' => 'no-reply@qualhon.com',
                    'to' => $to,
                    'subject' => $subject,
                    'html' => $html,
                    'text' => $plain
                    )
                );
                $j = json_decode(curl_exec($ch));
                $info = curl_getinfo($ch);
                if($info['http_code'] != 200)
                    
                curl_close($ch);  
                
                    
            }

              }
        return $this->response('New art work has not been included in the artwork and link for the image has been sent !!');     
       
    }

    public function app_access_time(){
    
        $access_time=DB::table('app_timer')->select('from_dt','to_dt')->get();
        return $this->response($access_time);
    }

    

   

}
