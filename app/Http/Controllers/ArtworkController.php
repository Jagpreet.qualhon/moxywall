<?php

namespace App\Http\Controllers;

use DB;
use App\Artwork;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ArtworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $canva=DB::table('artworks')->get();
        return view('introscreens.gallery')->with(['canvas_list'=>$canva]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('introscreens.Add_image');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'mimes:jpeg,jpg,png,gif|required',

            ]);

        $cover = $request->file('image');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));
        // print_r($cover->getFilename().'.'.$extension);

        // echo $request->image_name;
        // exit;
         $img=$cover->getFilename().'.'.$extension;


         DB::table('artworks')->insert(['image'=>$img,'user'=>$request->image_name,'status'=>$request->status,'featured'=>0]);

        return back()->with('success','Intro Screen has been added successfully !! ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artwork  $artwork
     * @return \Illuminate\Http\Response
     */
    public function show(Artwork $artwork)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artwork  $artwork
     * @return \Illuminate\Http\Response
     */
    public function edit(Artwork $artwork)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artwork  $artwork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->status==1)
         {
             $s=0;
         }else
         {
            $s=1;
         }
         DB::table('artworks')->where('c_id',$request->id)->update(['status'=>$s]);

        return $s;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artwork  $artwork
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       $ids = $request->ids;
        DB::table("artworks")->whereIn('c_id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Art work has been Deleted successfully."]);
    }

    public function delete(Request $request) {
        DB::table('artworks')->where('c_id',$request->id)->delete();
        return redirect('artwork');
    }

    public function filter_status(Request $request){

           if($request->status =='0'){
            $canva = DB::table('artworks')
            ->select('*')
            ->where('status', $request->status)
            ->get();
           }else if($request->status =='1'){
            $canva = DB::table('artworks')
            ->select('*')
            ->where('status', $request->status)
            ->get();
           }else{
            $canva=DB::table('artworks')->get();
           }
           return view('introscreens.gallery')->with(['canvas_list'=>$canva]);

    }


    public function filter_status1(Request $request){

        if($request->status =='0'){
         $canva = DB::table('artworks')
         ->select('*')
         ->where('status', $request->status)
         ->get();
        }else if($request->status =='1'){
         $canva = DB::table('artworks')
         ->select('*')
         ->where('status', $request->status)
         ->get();
        }else{
         $canva=DB::table('artworks')->get();
        }
        return view('introscreens.introscreen')->with(['canvas_list'=>$canva]);

 }



public function introscreen(){

    $canva=DB::table('artworks')->where('featured', '1')->get();
    $introcount = count($canva);

    $allcanva=DB::table('artworks')->where('status','1')->get();
    return view('introscreens.introscreen')->with(['canvas_list'=>$canva, 'allcanva'=> $allcanva, 'introcount'=>$introcount]);



}

public function add_to_intro(Request $request){

    
    $ids = $request->ids;
   // print_r($ids);
   DB::table("artworks")->whereIn('c_id',explode(",",$ids))->update(['featured'=>'1']);
  
 return response()->json(['success'=>"Images added to Intro Screen Successfully."]);
    



}

public function remove_from_intro(Request $request){

    
    $ids = $request->ids;
   // print_r($ids);
   DB::table("artworks")->whereIn('c_id',explode(",",$ids))->update(['featured'=>'0']);
  
 return response()->json(['success'=>"Images removed from Intro Screen Successfully."]);
    



}

public function remove_feat(Request $request){

    
    DB::table('artworks')->where('c_id',$request->id)->update(['featured'=>0]);

    return response()->json(['success'=>"Image has been removed from Intro Screen Successfully."]);
  

}

public function chk(){
    $canva=DB::table('artworks')->where('featured', '1')->get();
    $introcount = count($canva);

    $allcanva=DB::table('artworks')->where('status','1')->get();
    return view('introscreens.chk')->with(['canvas_list'=>$canva, 'allcanva'=> $allcanva, 'introcount'=>$introcount]);


}

public function countselcted(){


}



}
