<?php

namespace App\Http\Controllers;


use DB;
use App\FrontEndUser;
use Illuminate\Http\Request;

class FrontEndUserController extends Controller
{
   public function index(){

      $users = FrontEndUser::all();
    //  $country=DB::table('countrylist')->select('*')->get();
      //echo "<pre>";print_r($country);
     return view('frontuser.userlist')->with(['users' => $users]);
   }

   public function update_user(Request $request, $id){

      $this->validate($request,[
         'title' =>'required',
         'email' => 'required',
         'first_name' =>'required',
         'last_name' =>'required',
         'country' =>'required',


         ]);

      $user = DB::table('frontusers')
      ->where('id',$id)
      ->update([
         'title'=>$request->title,
         'email'=> $request->email,
         'first_name'=> $request->first_name,
         'last_name'=> $request->last_name,
       'country'=> $request->country
         

          
              ]);
   
  return back()->with('success','User details has been updated successfully  !! ');


     }

     public function delete_user(Request $request) {

      DB::table('frontusers')->where('id',$request->id)->delete();
      return redirect('users');


     }





}
