<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StickerCat extends Model
{
    protected $table = 'sticker_cat';
}
