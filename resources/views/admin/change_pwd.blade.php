@extends('template.header')

@section('content')

<style>
a.eyeicon.pull-right {
    top: -32px;
    position: relative;right: 12px;
}
.eyeicon :hover{color:#bf028e;}
</style>

<div class="content-body">

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="card">
                <div class="card-body">
                <div class="card"><h4 class="card-title">Change Password</h4></div>
                    <div class="form-validation">
                        <form class="form-valide" method="post" action="{{url('/update_pwd')}}" enctype="multipart/form-data">

                            @csrf

<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="current_password">Current Password<span class="text-danger">*</span>
    </label>
    
    <div class="col-lg-6" id="show_hide_password">
    
      <input class="form-control" type="password" name="current_password" placeholder="Current Password" required>
            <div class="input-group-addon">
                <a class="eyeicon pull-right" href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
            </div>
    
	</div>
     
    
    
</div>

<div class="form-group row">
        <label class="col-lg-4 col-form-label" for="image_name">New Password<span class="text-danger">*</span>
        </label>
    <div class="col-lg-6" id="show_hide_password1">
            <input type="password" class="form-control" id="password" name="password" placeholder="New Password" required>
    <div class="input-group-addon">
        <a class="eyeicon pull-right" href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
        </div>                     
             </div>
                          
                          
</div>



                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="image_name">Confirm Password<span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6" id="show_hide_password2">
<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required>
<div class="input-group-addon">
<a class="eyeicon pull-right" href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
      </div>                               
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Reset</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->
</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
    $j("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($j('#show_hide_password input').attr("type") == "text"){
            $j('#show_hide_password input').attr('type', 'password');
            $j('#show_hide_password i').addClass( "fa-eye-slash" );
            $j('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $j('#show_hide_password input').attr('type', 'text');
            $j('#show_hide_password i').removeClass( "fa-eye-slash" );
            $j('#show_hide_password i').addClass( "fa-eye" );
        }
    });


    $j("#show_hide_password1 a").on('click', function(event) {
        event.preventDefault();
        if($j('#show_hide_password1 input').attr("type") == "text"){
            $j('#show_hide_password1 input').attr('type', 'password');
            $j('#show_hide_password1 i').addClass( "fa-eye-slash" );
            $j('#show_hide_password1 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password1 input').attr("type") == "password"){
            $j('#show_hide_password1 input').attr('type', 'text');
            $j('#show_hide_password1 i').removeClass( "fa-eye-slash" );
            $j('#show_hide_password1 i').addClass( "fa-eye" );
        }
    });

    $j("#show_hide_password2 a").on('click', function(event) {
        event.preventDefault();
        if($j('#show_hide_password2 input').attr("type") == "text"){
            $j('#show_hide_password2 input').attr('type', 'password');
            $j('#show_hide_password2 i').addClass( "fa-eye-slash" );
            $j('#show_hide_password2 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password2 input').attr("type") == "password"){
            $j('#show_hide_password2 input').attr('type', 'text');
            $j('#show_hide_password2 i').removeClass( "fa-eye-slash" );
            $j('#show_hide_password2 i').addClass( "fa-eye" );
        }
    });




});

</script>

<script>
var $j=jQuery.noConflict();

$j(document).ready(function () {
 
window.setTimeout(function() {
    $j(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 4000);
 
});

</script>




@endsection
