@extends('template.header')

@section('content')
<div class="content-body">

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="card">
                <div class="card-body">
                <div class="card"><h4 class="card-title">Add Image For Canvas Background</h4></div>
                    <div class="form-validation">
                        <form class="form-valide" method="post" action="{{url('/add_canvas_image')}}" enctype="multipart/form-data">

                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="image_name">Image Name <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="image_name"  required name="image_name" placeholder="Image Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="myfile">Upload Image <span class="text-danger">*</span>
                                </label>

                                <div class="col-lg-6">
                                 <input id="image" name="image"  type="file" required class="" accept="image/*">
                    <!-- <div class="custom-file">

                        <input id="myfile" name="myfile"  type="file" required class="custom-file-input">
                        <label class="custom-file-label" accept="image/*">Choose file</label>
                    </div> -->
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="status">Status <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <select class="form-control"  required id="status" name="status">
                                        <option value="">Please select</option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>

                                    </select>
                                </div>
                            </div>




                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->
</div>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
 ></script>
 <script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>

@endsection
