@extends('template.header')

@section('content')
<style>

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}
#img01 { width: 300px; /* Full width */
  height: 300px; /* Full height */}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 9999999; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
<div class="content-body">
<div class="box"><meta name="csrf-token" content="{{ csrf_token() }}">

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Canvas List</h4>
                    <button style="float:right;"type="button" class="btn mb-1 btn-primary" onclick="window.location.href='{{url('Add_image')}}'">Add image</button>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                   
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    

                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($canvas_list)){ 
         
                         foreach ($canvas_list as $dn) { ?>
                            <tr>

                              <td><img class="img" style="border: 1px solid #ddd;
                              border-radius: 4px;
                              padding: 5px;
                              width: 50px;height: 50px;" src="{{asset('/storage/app/public')}}/{{$dn->image}}"></td>
                              <td>{{$dn->user}}</td>


                              <td> @if($dn->status==1)
                              <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}"> 
                              <button type="button" class="btn mb-1 btn-success st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Active</button>
                              @else
                              <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}"> 
                              <button type="button" class="btn mb-1 btn-danger st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Inactive</button>

                              @endif</td>
                                   
                                </tr>
                              
                              
                                <?php } } ?>
                           
                            </tbody>
                            <tfoot>
                                <tr>
                             
                                <th>Image</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<!-- #/ container -->
</div>

<script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

<script src="{{asset('assets/plugins/tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/tables/js/datatable-init/datatable-basic.min.js')}}"></script>
   
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script>
    var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption

var modalImg = document.getElementById("img01");
// var captionText = document.getElementById("caption");
var $j=jQuery.noConflict();

$j('.img').click(function(){
  modal.style.display = "block";
  modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
});

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}


    function change_status(val2){
          
swal({
title: "Are you sure?",
text: "Do you really want to change status ?",
type: "warning",
showCancelButton: true,
confirmButtonClass: "btn-danger",
confirmButtonText: "Yes, change it!",
closeOnConfirm: false
},
function(){
var val=$j('#std_'+val2).val();
          //  alert(val);
          $.ajax({
              headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
          type: 'POST',
          url: "{{url('/change_status')}}",
          data: {status: val,id:val2},
          success: function(result){
           if(result==1)
           {
              $j('.st_'+val2).removeClass("btn-danger");

              $j('.st_'+val2).addClass("btn-success");
              $j('.st_'+val2).text('Active');
              $j('#std_'+val2).val(result);
           }
           else{
            $j('.st_'+val2).removeClass("btn-success");

            $j('.st_'+val2).addClass("btn-danger");
            $j('.st_'+val2).text('Inactive');

            $j('#std_'+val2).val(result);

           }                
          }
          });
          swal("Changed!", "status has been Changed.", "success");

});         

        
}



    </script>
@endsection
