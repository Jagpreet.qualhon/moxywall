@extends('template.header')

@section('content')
<link rel="stylesheet" href="dist/css/lightbox.min.css">

<style type="text/css">

.img-block-wrap a.example-image-link img {
    margin-bottom: 16px;
}
.img-block-wrap {
    margin-bottom: 30px;
    text-align: center;
}
span.lb-caption {
    display: none !important;
}
.example-image-link{display:block;}
.delete-grp {
    float: right;
    position: absolute;
    top: 93px;
    right: 29px;
}
.delete-grp span {
    margin-left: 10px;
}
.status-drop {
    float: right;
    width: auto;
}
.ckbx-img {
    position: absolute;
    top: 9px;
    right: 24px;
}
input[type="checkbox"]:checked:after {
    color: #bf028e;
    border: 1px solid #ddd;
}
button#cancelbtn {
    border: 1px solid #bf028e;
    background: #fff !important;
    color: #bf028e;
}


</style>


<div class="content-body">
<meta name="csrf-token" content="{{ csrf_token() }}">

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="{{ url('/gallery') }}">Canvas Background</a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
            <div class="card-body row">
            <div class="col-md-4">
                  <h4 style="margin-left: -15px;" class="card-title">Canvas Background</h4>
                  </div>

    <div class="col-md-4">
                        
                        </div>

                  <div class="col-md-4">
                  <button style="float:right;"type="button" class="btn mb-1 btn-primary" onclick="window.location.href='{{url('Add_image')}}'">Add image</button>
                  </div>

                  </div>

                  <div class="form-group filter-data col-md-4">
    <form id="status-form" role="form" method="get" action="{{url('/filter_status')}}">
<label style="margin-top: 10px;">Filter Canvas By Status</label>
                    <select onchange="this.form.submit()" class="form-control status-drop" name="status" id="status" >
                       
                        <option @if(Request::get('status')=='all') selected @endif value="all">All</option>
                        <option @if(Request::get('status')=='1') selected @endif value="1">Active Canvas</option>
                        <option @if(Request::get('status')=='0') selected @endif value="0">InActive Canvas</option>
                        
                    </select>
                    </form>
                </div>                 
                <div class="container">
               
<form class="form-valide" method="get" action="{{url('/deleteAll')}}" enctype="multipart/form-data">
@csrf

<div class="delete-grp">
<!-- <input type="checkbox" id="checkAll"><span>Select All</span>

<button style="display:none;" class="btn btn-primary delete_all" type="button" name="bulk_delete" id="bulk_delete">Delete</button> -->

<button class="btn btn-primary bulkselect pull-right" type="button" name="bulkselect" id="bulkselect">Bulk selection to delete</button>


<div style="display:none;" class="pull-right">
<button class="btn btn-primary delete_all" type="button" name="bulk_delete" id="bulk_delete">Delete</button>

<button  class="btn btn-primary cancelbtn" type="button" name="cancelbtn" id="cancelbtn">Cancel</button>
</div>


</div>

<div class="row">
    <?php if(!empty($canvas_list)){

         foreach ($canvas_list as $dn) { ?>


<div class="col-md-3 col-sm-6 col-xs-12 img-block-wrap" id="{{$dn->c_id}}">
<input style="display:none;" type="checkbox" class="image_chk image_chk_rm ckbx-img" id="check_all" name="img_ids[{{$dn->c_id}}]" data-id="{{$dn->c_id}}">
      <a class="example-image-link"
       href="{{asset('/storage/app/public')}}/{{$dn->image}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img  style="border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 250px;height: 120px; max-width:100%;"
            src="{{asset('/storage/app/public')}}/{{$dn->image}}"
            class="example-image" src="{{asset('/storage/app/public')}}/{{$dn->image}}" alt=""/></a>

       @if($dn->status==1)
    <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}">
    <button type="button" class="btn mb-1 btn-success st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Active</button>
    @else
    <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}">
    <button type="button" class="btn mb-1 btn-danger st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Inactive</button>

    @endif
    <a href="" class="btn mb-1 btn-danger button" data-id="{{$dn->c_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>


    </div>

          <?php } } ?>
          </div>
          </div>
          </form>


  </div>
                </div>
            </div>
        </div>
    </div>
      </div>
      </div>

</div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
var $j=jQuery.noConflict();
// $j('#checkAll').click(function () {
//     if (this.checked) {
//         $j('#bulk_delete').show();
//      $j('.image_chk').show();   
//      $j('.image_chk').prop('checked', true);
//     } else {
//         $j('.image_chk').hide();  
//         $j('#bulk_delete').hide();
//         $j('.image_chk').prop('checked', false);
//     }
//  });

 
 $j('#bulkselect').click(function () {
    
    // if (this.clicked) {
        $j(".pull-right").show();
        // $j(".remove_introcls").show();
        $j("#bulkselect").hide();
        $j(".image_chk_rm").show();
     $j('.image_chk_rm').prop('checked', true);
    // } else {
    //     $j('.image_chk_rm').prop('checked', false);
    //     $j(".image_chk_rm").hide();
    // }
 });


 $j('.cancelbtn').click(function () {
    $j(".pull-right").hide();
    $j(".image_chk_rm").hide();
    $j("#bulkselect").show();
 });




    $j('#bulk_delete').on('click', function(e) {
            var allIds = [];
            $j(".image_chk:checked").each(function() {
                allIds.push($(this).attr('data-id'));
            });
            if(allIds.length <=0)
            {
                swal("Error!", "Please select atleast one canvas to delete", "error");

            }else{
            swal({
            title: "Are you sure!",
            text: "You want to delete selected canvas images" ,
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
                    var strIds = allIds.join(",");
                    //alert(strIds);
                    $.ajax({
                        url: "{{ url('/deleteAll') }}",
                        type: 'GET',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+strIds,
                        success: function (data) {
                            if(data.success) {
                                let imgArr = strIds.split(',');
                                $.each( imgArr, function(key,value ) {
                                    $('#'+value).remove();
                                });
                            }
                            window.setTimeout(function() {
                            window.location.href ="{{url('/gallery')}}"
                        }, 1000);
                            swal("Deleted!", "Canvas Images has been Deleted.", "success");
                        },
                        error: function (data) {
                            //alert(data.responseText);
                        }
                    });
                });
            }

    });





</script>


<script>

function change_status(val2){
    var $j=jQuery.noConflict();
          swal({
          title: "Are you sure?",
          text: "Do you really want to change status ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, change it!",
          closeOnConfirm: false
          },
          function(){
          var val=$j('#std_'+val2).val();
                    //  alert(val);
                    $.ajax({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    type: 'POST',
                    url: "{{url('/change_status')}}",
                    data: {status: val,id:val2},
                    success: function(result){
                     if(result==1)
                     {
                        $j('.st_'+val2).removeClass("btn-danger");

                        $j('.st_'+val2).addClass("btn-success");
                        $j('.st_'+val2).text('Active');
                        $j('#std_'+val2).val(result);
                     }
                     else{
                      $j('.st_'+val2).removeClass("btn-success");

                      $j('.st_'+val2).addClass("btn-danger");
                      $j('.st_'+val2).text('Inactive');

                      $j('#std_'+val2).val(result);

                     }
                    }
                    });
                    swal("Changed!", "Status has been Changed.", "success");

          });


          }

</script>

<script type="text/javascript">
  var $s=jQuery.noConflict();
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "You want to delete this canvas image" ,
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                type: "GET",
                url: "{{url('/delete_canvas')}}",
                data: {id:id},
                success: function (data) {

                    swal("Deleted!", "Canvas Image has been Deleted.", "success");
                    window.setTimeout(function() {
                            window.location.href ="{{url('/gallery')}}"
                        }, 1000);
                    }

            });
    });
});

</script>


  <script src="dist/js/lightbox-plus-jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>



  @endsection
