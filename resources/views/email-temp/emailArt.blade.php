<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
<title></title>
<style>
.wrapper {
	width: 100%;
	table-layout: fixed;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}
</style>
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
    <center class="wrapper" style="width:100%;table-layout:fixed;
    -webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fff;">
    <table style="border: 10px solid #000;background: #fff;margin: 50px 0;padding: 40px 54px 24px;width: 700px;">
    <tbody>
        <tr>
            <td>
              <table style="width: 166px;margin:0 auto;padding-bottom: 16px;">
                  <tbody>
                      <tr>
                          <td>
                                <img src="images/Moxy_East_Village_Logo.png" style="max-width:100%">
                          </td>
                      </tr>
                  </tbody>
              </table>
            </td>   
        </tr>
        <tr>
            <td>
                <h2 style="font-family: 'Open Sans', sans-serif;text-align: center;color: #aa0b79;font-weight: 400;font-size: 34px;
                letter-spacing: 1px;">HI THERE!</h2>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-family: 'Open Sans', sans-serif;margin: 20px 0 18px;font-size: 16px;"><strong>Dear Guest,</strong></p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin-bottom: 11px;">Thank you for visiting us at Moxy NYC East Village! Please use the link below to
                     access your artwork and enjoy
                    sharing it with your friends and family.
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="ffont-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">Artwork link: <a href="javascript:void(0);" style="color: #075dfc;font-weight: 600;">https://EXAMPLE</a></p>
            </td>
        </tr>
        <tr>
            <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 11px 0 0;">We hope to see you again soon.</p></td>
        </tr>
        <tr>
            <td>
                    <table style="margin:0 auto;margin-top: 100px;">
                            <tbody>
                                <tr>
                                    <td>
                                          <p style="font-family: 'Open Sans', sans-serif;font-size: 13px;margin:0;">Please do not reply to this message as we will be not able to recive your request</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
            </td>
        </tr>
    </tbody>
</table>
</center>
</body>
</html>