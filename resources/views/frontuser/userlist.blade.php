@extends('template.header')

@section('content')
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
<link href="https://cdn.datatables.net/colreorder/1.5.2/css/colReorder.dataTables.min.css">
<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
<script type="text/javascript">
var $j =jQuery.noConflict();
$j(document).ready(function() {
    $j('#example').DataTable( {
      "lengthChange": false
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>

<style>
input[type="search"] {
    border: 1px solid #dee2e6;
    border-radius: 5px;
}
.cap-text{text-transform: capitalize;}
button.dt-button {
    background: #bf028e;
    color: #fff;
    border: #bf028e;
}
div#data_exmp_wrapper > div#data_exmp_filter {
    display: none;
}
div#data_exmp_wrapper .row:last-of-type {
    display: none;
}
/* .dt-buttons, h4.card-title{
    padding-left: 30px;
} */
a.paginate_button.current {
    background: #bf028e;
    color: #fff;
}
div#data_exmp_info {
    margin-left: 30px;
}
div#data_exmp_paginate {
    display: inline-block;
    float: right;
    margin-right: 30px;
}
.page-item.active .page-link {
   
    background-color: #bf028e;
    border-color: #bf028e;
}
.page-link{color:#bf028e}

</style>
<div class="content-body">
<div class="box">
<meta name="csrf-token" content="{{ csrf_token() }}">

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif






<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Users List</h4>
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration" id="example">
                            <thead>
                                <tr>
                                   
                                   <th>Title</th>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Email</th>
                                    <th>Edit/Delete</th>
                                    

                                </tr>


                            </thead>
                            <tbody>
                            <?php if(!empty($users)){ 
         
                         foreach ($users as $u) { ?>
                            <tr>

                              <td class="cap-text">{{ $u->title }}</td>
                                <td class="cap-text">{{$u->first_name}}   {{ $u->last_name }}</td>
                                  <td class="cap-text">{{ $u->country }}</td>
                                    <td>{{ $u->email }}</td>
                                    <td>
     <a href="" data-toggle="modal" data-target="#basicExampleModal{{ $u->id }}" class="btn mb-1 btn-success" data_value="{{ $u->id}}" ><i class="fa fa-edit" aria-hidden="true"></i></a>
     
     <a href="" class="btn mb-1 btn-danger button" data-id="{{$u->id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
     </td>
                                   
                            </tr>
<!-- edit user modal start -->


<div class="modal fade" id="basicExampleModal{{ $u->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      <div class="modal-header">
      
        <h5 class="modal-title" id="exampleModalLabel">Update {{ $u->first_name }} Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" > 
      <div class="form-validation">
  <form class="form-valide" method="post" action="{{url('/update_user')}}/{{$u->id}}" enctype="multipart/form-data">

                            @csrf

<div class="form-group row">
  <label class="col-lg-4 col-form-label" for="first_name">Title<span class="text-danger">*</span>
  </label>
  <div class="col-lg-6">
  <select class="form-control"  required id="title" name="title">
            <option value="" disabled selected>Select Title</option>
            <option <?php if($u->title == 'Mr'){ ?> selected <?php } ?> value="Mr">Mr</option>
            <option <?php if($u->title == 'Ms'){ ?> selected <?php } ?> value="Ms">Ms</option>
            <option <?php if($u->title == 'Mrs'){ ?> selected <?php } ?> value="Mrs">Mrs</option>
          
          

            </select>
  </div>
</div>

<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="first_name">First Name<span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
    <input class="form-control" name="first_name" type="text" value="{{$u->first_name}}" placeholder="Add First Name" required>
    </div>
</div>

<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="first_name">Last Name<span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
    <input class="form-control" name="last_name" type="text" value="{{$u->last_name}}" placeholder="Add Last Name" required>
    </div>
</div>

<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="first_name">Email<span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
    <input class="form-control" name="email" type="email" value="{{$u->email}}" placeholder="Add Email" required>
    </div>
</div>



<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="first_name">Country<span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
  <select class="form-control" id="country" name="country" required>
       <option value="" disabled selected>Select Country</option>
       <option <?php if($u->country == 'Afghanistan'){ ?> selected <?php } ?>value="Afghanistan">Afghanistan</option>
       <option <?php if($u->country == 'Albania'){ ?> selected <?php } ?> value="Albania">Albania</option>
       <option <?php if($u->country == 'Algeria'){ ?> selected <?php } ?> value="Algeria">Algeria</option>
       <option <?php if($u->country == 'American Samoa'){ ?> selected <?php } ?> value="American Samoa">American Samoa</option>
       <option <?php if($u->country == 'Andorra'){ ?> selected <?php } ?> value="Andorra">Andorra</option>
       <option <?php if($u->country == 'Angola'){ ?> selected <?php } ?> value="Angola">Angola</option>
       <option <?php if($u->country == 'Anguilla'){ ?> selected <?php } ?> value="Anguilla">Anguilla</option>
       <option <?php if($u->country == 'Antigua & Barbuda'){ ?> selected <?php } ?> value="Antigua & Barbuda">Antigua & Barbuda</option>
       <option <?php if($u->country == 'Argentina'){ ?> selected <?php } ?> value="Argentina">Argentina</option>
       <option <?php if($u->country == 'Armenia'){ ?> selected <?php } ?> value="Armenia">Armenia</option>
       <option <?php if($u->country == 'Aruba'){ ?> selected <?php } ?> value="Aruba">Aruba</option>
       <option <?php if($u->country == 'Australia'){ ?> selected <?php } ?> value="Australia">Australia</option>
       <option <?php if($u->country == 'Austria'){ ?> selected <?php } ?> value="Austria">Austria</option>
       <option <?php if($u->country == 'Azerbaijan'){ ?> selected <?php } ?> value="Azerbaijan">Azerbaijan</option>
       <option <?php if($u->country == 'Bahamas'){ ?> selected <?php } ?> value="Bahamas">Bahamas</option>
       <option <?php if($u->country == 'Bahrain'){ ?> selected <?php } ?> value="Bahrain">Bahrain</option>
       <option <?php if($u->country == 'Bangladesh'){ ?> selected <?php } ?> value="Bangladesh">Bangladesh</option>
       <option <?php if($u->country == 'Barbados'){ ?> selected <?php } ?> value="Barbados">Barbados</option>
       <option <?php if($u->country == 'Belarus'){ ?> selected <?php } ?> value="Belarus">Belarus</option>
       <option <?php if($u->country == 'Belgium'){ ?> selected <?php } ?> value="Belgium">Belgium</option>
       <option <?php if($u->country == 'Belize'){ ?> selected <?php } ?> value="Belize">Belize</option>
       <option <?php if($u->country == 'Benin'){ ?> selected <?php } ?> value="Benin">Benin</option>
       <option <?php if($u->country == 'Bermuda'){ ?> selected <?php } ?> value="Bermuda">Bermuda</option>
       <option <?php if($u->country == 'Bhutan'){ ?> selected <?php } ?> value="Bhutan">Bhutan</option>
       <option <?php if($u->country == 'Bolivia'){ ?> selected <?php } ?> value="Bolivia">Bolivia</option>
       <option <?php if($u->country == 'Bonaire'){ ?> selected <?php } ?> value="Bonaire">Bonaire</option>
       <option <?php if($u->country == 'Bosnia & Herzegovina'){ ?> selected <?php } ?> value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
       <option <?php if($u->country == 'Botswana'){ ?> selected <?php } ?> value="Botswana">Botswana</option>
       <option <?php if($u->country == 'Brazil'){ ?> selected <?php } ?> value="Brazil">Brazil</option>
       <option <?php if($u->country == 'British Indian Ocean Ter'){ ?> selected <?php } ?> value="British Indian Ocean Ter">British Indian Ocean Ter</option>
       <option <?php if($u->country == 'Brunei'){ ?> selected <?php } ?> value="Brunei">Brunei</option>
       <option <?php if($u->country == 'Bulgaria'){ ?> selected <?php } ?>  value="Bulgaria">Bulgaria</option>
       <option  <?php if($u->country == 'Burkina Faso'){ ?> selected <?php } ?> value="Burkina Faso">Burkina Faso</option>
       <option <?php if($u->country == 'Burundi'){ ?> selected <?php } ?> value="Burundi">Burundi</option>
       <option <?php if($u->country == 'Cambodia'){ ?> selected <?php } ?> value="Cambodia">Cambodia</option>
       <option <?php if($u->country == 'Cameroon'){ ?> selected <?php } ?> value="Cameroon">Cameroon</option>
       <option <?php if($u->country == 'Canada'){ ?> selected <?php } ?> value="Canada">Canada</option>
       <option <?php if($u->country == 'Canary Islands'){ ?> selected <?php } ?> value="Canary Islands">Canary Islands</option>
       <option <?php if($u->country == 'Cape Verde'){ ?> selected <?php } ?> value="Cape Verde">Cape Verde</option>
       <option <?php if($u->country == 'Cayman Islands'){ ?> selected <?php } ?> value="Cayman Islands">Cayman Islands</option>
       <option <?php if($u->country == 'Central African Republic'){ ?> selected <?php } ?> value="Central African Republic">Central African Republic</option>
       <option <?php if($u->country == 'Chad'){ ?> selected <?php } ?>  value="Chad">Chad</option>
       <option <?php if($u->country == 'Channel Islands'){ ?> selected <?php } ?>  value="Channel Islands">Channel Islands</option>
       <option  <?php if($u->country == 'Chile'){ ?> selected <?php } ?> value="Chile">Chile</option>
       <option <?php if($u->country == 'China'){ ?> selected <?php } ?>  value="China">China</option>
       <option <?php if($u->country == 'Christmas Island'){ ?> selected <?php } ?>  value="Christmas Island">Christmas Island</option>
       <option  <?php if($u->country == 'Cocos Island'){ ?> selected <?php } ?> value="Cocos Island">Cocos Island</option>
       <option <?php if($u->country == 'Colombia'){ ?> selected <?php } ?> value="Colombia">Colombia</option>
       <option <?php if($u->country == 'Comoros'){ ?> selected <?php } ?>  value="Comoros">Comoros</option>
       <option <?php if($u->country == 'Congo'){ ?> selected <?php } ?> value="Congo">Congo</option>
       <option <?php if($u->country == 'Cook Islands'){ ?> selected <?php } ?>  value="Cook Islands">Cook Islands</option>
       <option <?php if($u->country == 'Costa Rica'){ ?> selected <?php } ?>  value="Costa Rica">Costa Rica</option>
       <option <?php if($u->country == 'Cote DIvoire'){ ?> selected <?php } ?> value="Cote DIvoire">Cote DIvoire</option>
       <option <?php if($u->country == 'Croatia'){ ?> selected <?php } ?> value="Croatia">Croatia</option>
       <option <?php if($u->country == 'Cuba'){ ?> selected <?php } ?>  value="Cuba">Cuba</option>
       <option <?php if($u->country == 'Curacao'){ ?> selected <?php } ?> value="Curaco">Curacao</option>
       <option <?php if($u->country == 'Cyprus'){ ?> selected <?php } ?> value="Cyprus">Cyprus</option>
       <option <?php if($u->country == 'Czech Republic'){ ?> selected <?php } ?> value="Czech Republic">Czech Republic</option>
       <option <?php if($u->country == 'Denmark'){ ?> selected <?php } ?> value="Denmark">Denmark</option>
       <option <?php if($u->country == 'Djibouti'){ ?> selected <?php } ?> value="Djibouti">Djibouti</option>
       <option <?php if($u->country == 'Dominica'){ ?> selected <?php } ?> value="Dominica">Dominica</option>
       <option <?php if($u->country == 'Dominican Republic'){ ?> selected <?php } ?> value="Dominican Republic">Dominican Republic</option>
       <option <?php if($u->country == 'East Timor'){ ?> selected <?php } ?> value="East Timor">East Timor</option>
       <option <?php if($u->country == 'Ecuador'){ ?> selected <?php } ?> value="Ecuador">Ecuador</option>
       <option <?php if($u->country == 'Egypt'){ ?> selected <?php } ?> value="Egypt">Egypt</option>
       <option <?php if($u->country == 'El Salvador'){ ?> selected <?php } ?> value="El Salvador">El Salvador</option>
       <option <?php if($u->country == 'Equatorial Guinea'){ ?> selected <?php } ?> value="Equatorial Guinea">Equatorial Guinea</option>
       <option <?php if($u->country == 'Eritrea'){ ?> selected <?php } ?> value="Eritrea">Eritrea</option>
       <option <?php if($u->country == 'Estonia'){ ?> selected <?php } ?> value="Estonia">Estonia</option>
       <option <?php if($u->country == 'Ethiopia'){ ?> selected <?php } ?> value="Ethiopia">Ethiopia</option>
       <option <?php if($u->country == 'Falkland Islands'){ ?> selected <?php } ?> value="Falkland Islands">Falkland Islands</option>
       <option <?php if($u->country == 'Faroe Islands'){ ?> selected <?php } ?> value="Faroe Islands">Faroe Islands</option>
       <option <?php if($u->country == 'Fiji'){ ?> selected <?php } ?> value="Fiji">Fiji</option>
       <option <?php if($u->country == 'Finland'){ ?> selected <?php } ?> value="Finland">Finland</option>
       <option <?php if($u->country == 'France'){ ?> selected <?php } ?> value="France">France</option>
       <option <?php if($u->country == 'French Guiana'){ ?> selected <?php } ?> value="French Guiana">French Guiana</option>
       <option <?php if($u->country == 'French Polynesia'){ ?> selected <?php } ?> value="French Polynesia">French Polynesia</option>
       <option <?php if($u->country == 'French Southern Ter'){ ?> selected <?php } ?> value="French Southern Ter">French Southern Ter</option>
       <option <?php if($u->country == 'Gabon'){ ?> selected <?php } ?> value="Gabon">Gabon</option>
       <option <?php if($u->country == 'Gambia'){ ?> selected <?php } ?> value="Gambia">Gambia</option>
       <option <?php if($u->country == 'Georgia'){ ?> selected <?php } ?> value="Georgia">Georgia</option>
       <option <?php if($u->country == 'Germany'){ ?> selected <?php } ?> value="Germany">Germany</option>
       <option <?php if($u->country == 'Ghana'){ ?> selected <?php } ?> value="Ghana">Ghana</option>
       <option <?php if($u->country == 'Gibraltar'){ ?> selected <?php } ?> value="Gibraltar">Gibraltar</option>
       <option <?php if($u->country == 'Great Britain'){ ?> selected <?php } ?> value="Great Britain">Great Britain</option>
       <option <?php if($u->country == 'Greece'){ ?> selected <?php } ?> value="Greece">Greece</option>
       <option <?php if($u->country == 'Greenland'){ ?> selected <?php } ?> value="Greenland">Greenland</option>
       <option <?php if($u->country == 'Grenada'){ ?> selected <?php } ?> value="Grenada">Grenada</option>
       <option <?php if($u->country == 'Guadeloupe'){ ?> selected <?php } ?> value="Guadeloupe">Guadeloupe</option>
       <option <?php if($u->country == 'Guam'){ ?> selected <?php } ?> value="Guam">Guam</option>
       <option <?php if($u->country == 'Guatemala'){ ?> selected <?php } ?> value="Guatemala">Guatemala</option>
       <option <?php if($u->country == 'Guinea'){ ?> selected <?php } ?> value="Guinea">Guinea</option>
       <option <?php if($u->country == 'Guyana'){ ?> selected <?php } ?> value="Guyana">Guyana</option>
       <option <?php if($u->country == 'Haiti'){ ?> selected <?php } ?> value="Haiti">Haiti</option>
       <option <?php if($u->country == 'Hawaii'){ ?> selected <?php } ?> value="Hawaii">Hawaii</option>
       <option <?php if($u->country == 'Honduras'){ ?> selected <?php } ?> value="Honduras">Honduras</option>
       <option <?php if($u->country == 'Hong Kong'){ ?> selected <?php } ?> value="Hong Kong">Hong Kong</option>
       <option <?php if($u->country == 'Hungary'){ ?> selected <?php } ?> value="Hungary">Hungary</option>
       <option <?php if($u->country == 'Iceland'){ ?> selected <?php } ?> value="Iceland">Iceland</option>
       <option <?php if($u->country == 'Indonesia'){ ?> selected <?php } ?> value="Indonesia">Indonesia</option>
       <option <?php if($u->country == 'India'){ ?> selected <?php } ?> value="India">India</option>
       <option <?php if($u->country == 'Iran'){ ?> selected <?php } ?> value="Iran">Iran</option>
       <option <?php if($u->country == 'Iraq'){ ?> selected <?php } ?> value="Iraq">Iraq</option>
       <option <?php if($u->country == 'Ireland'){ ?> selected <?php } ?> value="Ireland">Ireland</option>
       <option <?php if($u->country == 'Isle of Man'){ ?> selected <?php } ?> value="Isle of Man">Isle of Man</option>
       <option <?php if($u->country == 'Israel'){ ?> selected <?php } ?> value="Israel">Israel</option>
       <option <?php if($u->country == 'Italy'){ ?> selected <?php } ?> value="Italy">Italy</option>
       <option <?php if($u->country == 'Jamaica'){ ?> selected <?php } ?> value="Jamaica">Jamaica</option>
       <option <?php if($u->country == 'Japan'){ ?> selected <?php } ?> value="Japan">Japan</option>
       <option <?php if($u->country == 'Jordan'){ ?> selected <?php } ?> value="Jordan">Jordan</option>
       <option <?php if($u->country == 'Kazakhstan'){ ?> selected <?php } ?> value="Kazakhstan">Kazakhstan</option>
       <option <?php if($u->country == 'Kenya'){ ?> selected <?php } ?> value="Kenya">Kenya</option>
       <option <?php if($u->country == 'Kiribati'){ ?> selected <?php } ?> value="Kiribati">Kiribati</option>
       <option <?php if($u->country == 'Korea North'){ ?> selected <?php } ?> value="Korea North">Korea North</option>
       <option <?php if($u->country == 'Korea South'){ ?> selected <?php } ?> value="Korea Sout">Korea South</option>
       <option <?php if($u->country == 'Kuwait'){ ?> selected <?php } ?> value="Kuwait">Kuwait</option>
       <option <?php if($u->country == 'Kyrgyzstan'){ ?> selected <?php } ?> value="Kyrgyzstan">Kyrgyzstan</option>
       <option <?php if($u->country == 'Laos'){ ?> selected <?php } ?> value="Laos">Laos</option>
       <option <?php if($u->country == 'Latvia'){ ?> selected <?php } ?> value="Latvia">Latvia</option>
       <option <?php if($u->country == 'Lebanon'){ ?> selected <?php } ?> value="Lebanon">Lebanon</option>
       <option <?php if($u->country == 'Lesotho'){ ?> selected <?php } ?> value="Lesotho">Lesotho</option>
       <option <?php if($u->country == 'Liberia'){ ?> selected <?php } ?> value="Liberia">Liberia</option>
       <option <?php if($u->country == 'Libya'){ ?> selected <?php } ?> value="Libya">Libya</option>
       <option <?php if($u->country == 'Liechtenstein'){ ?> selected <?php } ?> value="Liechtenstein">Liechtenstein</option>
       <option <?php if($u->country == 'Lithuania'){ ?> selected <?php } ?> value="Lithuania">Lithuania</option>
       <option <?php if($u->country == 'Luxembourg'){ ?> selected <?php } ?> value="Luxembourg">Luxembourg</option>
       <option <?php if($u->country == 'Macau'){ ?> selected <?php } ?> value="Macau">Macau</option>
       <option <?php if($u->country == 'Macedonia'){ ?> selected <?php } ?> value="Macedonia">Macedonia</option>
       <option <?php if($u->country == 'Madagascar'){ ?> selected <?php } ?> value="Madagascar">Madagascar</option>
       <option <?php if($u->country == 'Malaysia'){ ?> selected <?php } ?> value="Malaysia">Malaysia</option>
       <option <?php if($u->country == 'Malawi'){ ?> selected <?php } ?> value="Malawi">Malawi</option>
       <option <?php if($u->country == 'Maldives'){ ?> selected <?php } ?> value="Maldives">Maldives</option>
       <option <?php if($u->country == 'Mali'){ ?> selected <?php } ?> value="Mali">Mali</option>
       <option <?php if($u->country == 'Malta'){ ?> selected <?php } ?>  value="Malta">Malta</option>
       <option <?php if($u->country == 'Marshall Islands'){ ?> selected <?php } ?>  value="Marshall Islands">Marshall Islands</option>
       <option <?php if($u->country == 'Martinique'){ ?> selected <?php } ?> value="Martinique">Martinique</option>
       <option <?php if($u->country == 'Mauritania'){ ?> selected <?php } ?> value="Mauritania">Mauritania</option>
       <option <?php if($u->country == 'Mauritius'){ ?> selected <?php } ?> value="Mauritius">Mauritius</option>
       <option <?php if($u->country == 'Mayotte'){ ?> selected <?php } ?> value="Mayotte">Mayotte</option>
       <option <?php if($u->country == 'Mexico'){ ?> selected <?php } ?> value="Mexico">Mexico</option>
       <option <?php if($u->country == 'Midway Islands'){ ?> selected <?php } ?> value="Midway Islands">Midway Islands</option>
       <option <?php if($u->country == 'Moldova'){ ?> selected <?php } ?>  value="Moldova">Moldova</option>
       <option <?php if($u->country == 'Monaco'){ ?> selected <?php } ?> value="Monaco">Monaco</option>
       <option <?php if($u->country == 'Mongolia'){ ?> selected <?php } ?> value="Mongolia">Mongolia</option>
       <option <?php if($u->country == 'Montserrat'){ ?> selected <?php } ?> value="Montserrat">Montserrat</option>
       <option <?php if($u->country == 'Morocco'){ ?> selected <?php } ?> value="Morocco">Morocco</option>
       <option <?php if($u->country == 'Mozambique'){ ?> selected <?php } ?> value="Mozambique">Mozambique</option>
       <option <?php if($u->country == 'Myanmar'){ ?> selected <?php } ?> value="Myanmar">Myanmar</option>
       <option <?php if($u->country == 'Nambia'){ ?> selected <?php } ?> value="Nambia">Nambia</option>
       <option <?php if($u->country == 'Nauru'){ ?> selected <?php } ?> value="Nauru">Nauru</option>
       <option <?php if($u->country == 'Nepal'){ ?> selected <?php } ?> value="Nepal">Nepal</option>
       <option <?php if($u->country == 'Netherland Antilles'){ ?> selected <?php } ?> value="Netherland Antilles">Netherland Antilles</option>
       <option <?php if($u->country == 'Netherlands(Holland, Europe)'){ ?> selected <?php } ?> value="Netherlands(Holland, Europe)">Netherlands(Holland, Europe)</option>
       <option <?php if($u->country == 'Nevis'){ ?> selected <?php } ?> value="Nevis">Nevis</option>
       <option <?php if($u->country == 'New Caledonia'){ ?> selected <?php } ?> value="New Caledonia">New Caledonia</option>
       <option <?php if($u->country == 'New Zealand'){ ?> selected <?php } ?> value="New Zealand">New Zealand</option>
       <option <?php if($u->country == 'Nicaragua'){ ?> selected <?php } ?> value="Nicaragua">Nicaragua</option>
       <option <?php if($u->country == 'Niger'){ ?> selected <?php } ?> value="Niger">Niger</option>
       <option <?php if($u->country == 'Nigeria'){ ?> selected <?php } ?> value="Nigeria">Nigeria</option>
       <option <?php if($u->country == 'Niue'){ ?> selected <?php } ?> value="Niue">Niue</option>
       <option <?php if($u->country == 'Norfolk Island'){ ?> selected <?php } ?> value="Norfolk Island">Norfolk Island</option>
       <option <?php if($u->country == 'Norway'){ ?> selected <?php } ?> value="Norway">Norway</option>
       <option <?php if($u->country == 'Oman'){ ?> selected <?php } ?> value="Oman">Oman</option>
       <option <?php if($u->country == 'Pakistan'){ ?> selected <?php } ?> value="Pakistan">Pakistan</option>
       <option <?php if($u->country == 'Palau Island'){ ?> selected <?php } ?> value="Palau Island">Palau Island</option>
       <option <?php if($u->country == 'Palestine'){ ?> selected <?php } ?> value="Palestine">Palestine</option>
       <option <?php if($u->country == 'Panama'){ ?> selected <?php } ?> value="Panama">Panama</option>
       <option <?php if($u->country == 'Papua New Guinea'){ ?> selected <?php } ?> value="Papua New Guinea">Papua New Guinea</option>
       <option <?php if($u->country == 'Paraguay'){ ?> selected <?php } ?> value="Paraguay">Paraguay</option>
       <option <?php if($u->country == 'Peru'){ ?> selected <?php } ?> value="Peru">Peru</option>
       <option <?php if($u->country == 'Phillipines'){ ?> selected <?php } ?> value="Phillipines">Philippines</option>
       <option <?php if($u->country == 'Pitcairn Island'){ ?> selected <?php } ?> value="Pitcairn Island">Pitcairn Island</option>
       <option <?php if($u->country == 'Poland'){ ?> selected <?php } ?> value="Poland">Poland</option>
       <option <?php if($u->country == 'Portugal'){ ?> selected <?php } ?>  value="Portugal">Portugal</option>
       <option <?php if($u->country == 'Puerto Rico'){ ?> selected <?php } ?> value="Puerto Rico">Puerto Rico</option>
       <option <?php if($u->country == 'Qatar'){ ?> selected <?php } ?> value="Qatar">Qatar</option>
       <option <?php if($u->country == 'Republic of Montenegro'){ ?> selected <?php } ?> value="Republic of Montenegro">Republic of Montenegro</option>
       <option <?php if($u->country == 'Republic of Serbia'){ ?> selected <?php } ?> value="Republic of Serbia">Republic of Serbia</option>
       <option <?php if($u->country == 'Reunion'){ ?> selected <?php } ?> value="Reunion">Reunion</option>
       <option <?php if($u->country == 'Romania'){ ?> selected <?php } ?> value="Romania">Romania</option>
       <option <?php if($u->country == 'Russia'){ ?> selected <?php } ?> value="Russia">Russia</option>
       <option <?php if($u->country == 'Rwanda'){ ?> selected <?php } ?> value="Rwanda">Rwanda</option>
       <option <?php if($u->country == 'St Barthelemy'){ ?> selected <?php } ?> value="St Barthelemy">St Barthelemy</option>
       <option <?php if($u->country == 'St Eustatius'){ ?> selected <?php } ?> value="St Eustatius">St Eustatius</option>
       <option <?php if($u->country == 'St Helena'){ ?> selected <?php } ?> value="St Helena">St Helena</option>
       <option <?php if($u->country == 'St Kitts-Nevis'){ ?> selected <?php } ?> value="St Kitts-Nevis">St Kitts-Nevis</option>
       <option <?php if($u->country == 'St Lucia'){ ?> selected <?php } ?> value="St Lucia">St Lucia</option>
       <option <?php if($u->country == 'St Maarten'){ ?> selected <?php } ?> value="St Maarten">St Maarten</option>
       <option <?php if($u->country == 'St Pierre & Miquelon'){ ?> selected <?php } ?> value="St Pierre & Miquelon">St Pierre & Miquelon</option>
       <option <?php if($u->country == 'St Vincent & Grenadines'){ ?> selected <?php } ?> value="St Vincent & Grenadines">St Vincent & Grenadines</option>
       <option <?php if($u->country == 'Saipan'){ ?> selected <?php } ?>  value="Saipan">Saipan</option>
       <option <?php if($u->country == 'Samoa'){ ?> selected <?php } ?> value="Samoa">Samoa</option>
       <option <?php if($u->country == 'Samoa American'){ ?> selected <?php } ?> value="Samoa American">Samoa American</option>
       <option <?php if($u->country == 'San Marino'){ ?> selected <?php } ?> value="San Marino">San Marino</option>
       <option <?php if($u->country == 'Sao Tome & Principe'){ ?> selected <?php } ?> value="Sao Tome & Principe">Sao Tome & Principe</option>
       <option <?php if($u->country == 'Saudi Arabia'){ ?> selected <?php } ?> value="Saudi Arabia">Saudi Arabia</option>
       <option <?php if($u->country == 'Senegal'){ ?> selected <?php } ?> value="Senegal">Senegal</option>
       <option <?php if($u->country == 'Seychelles'){ ?> selected <?php } ?> value="Seychelles">Seychelles</option>
       <option <?php if($u->country == 'Sierra Leone'){ ?> selected <?php } ?> value="Sierra Leone">Sierra Leone</option>
       <option <?php if($u->country == 'Singapore'){ ?> selected <?php } ?> value="Singapore">Singapore</option>
       <option <?php if($u->country == 'Slovakia'){ ?> selected <?php } ?> value="Slovakia">Slovakia</option>
       <option <?php if($u->country == 'Slovenia'){ ?> selected <?php } ?> value="Slovenia">Slovenia</option>
       <option <?php if($u->country == 'Solomon Islands'){ ?> selected <?php } ?> value="Solomon Islands">Solomon Islands</option>
       <option <?php if($u->country == 'Somalia'){ ?> selected <?php } ?> value="Somalia">Somalia</option>
       <option <?php if($u->country == 'South Africa'){ ?> selected <?php } ?> value="South Africa">South Africa</option>
       <option <?php if($u->country == 'Spain'){ ?> selected <?php } ?> value="Spain">Spain</option>
       <option <?php if($u->country == 'Sri Lanka'){ ?> selected <?php } ?> value="Sri Lanka">Sri Lanka</option>
       <option <?php if($u->country == 'Sudan'){ ?> selected <?php } ?> value="Sudan">Sudan</option>
       <option <?php if($u->country == 'Suriname'){ ?> selected <?php } ?> value="Suriname">Suriname</option>
       <option <?php if($u->country == 'Swaziland'){ ?> selected <?php } ?> value="Swaziland">Swaziland</option>
       <option <?php if($u->country == 'Sweden'){ ?> selected <?php } ?> value="Sweden">Sweden</option>
       <option <?php if($u->country == 'Switzerland'){ ?> selected <?php } ?> value="Switzerland">Switzerland</option>
       <option <?php if($u->country == 'Syria'){ ?> selected <?php } ?> value="Syria">Syria</option>
       <option <?php if($u->country == 'Tahiti'){ ?> selected <?php } ?> value="Tahiti">Tahiti</option>
       <option <?php if($u->country == 'Taiwan'){ ?> selected <?php } ?> value="Taiwan">Taiwan</option>
       <option <?php if($u->country == 'Tajikistan'){ ?> selected <?php } ?> value="Tajikistan">Tajikistan</option>
       <option <?php if($u->country == 'Tanzania'){ ?> selected <?php } ?> value="Tanzania">Tanzania</option>
       <option <?php if($u->country == 'Thailand'){ ?> selected <?php } ?> value="Thailand">Thailand</option>
       <option <?php if($u->country == 'Togo'){ ?> selected <?php } ?> value="Togo">Togo</option>
       <option <?php if($u->country == 'Tokelau'){ ?> selected <?php } ?> value="Tokelau">Tokelau</option>
       <option <?php if($u->country == 'Tonga'){ ?> selected <?php } ?> value="Tonga">Tonga</option>
       <option <?php if($u->country == 'Trinidad & Tobago'){ ?> selected <?php } ?> value="Trinidad & Tobago">Trinidad & Tobago</option>
       <option <?php if($u->country == 'Tunisia'){ ?> selected <?php } ?> value="Tunisia">Tunisia</option>
       <option <?php if($u->country == 'Turkey'){ ?> selected <?php } ?> value="Turkey">Turkey</option>
       <option <?php if($u->country == 'Turkmenistan'){ ?> selected <?php } ?> value="Turkmenistan">Turkmenistan</option>
       <option <?php if($u->country == 'Turks & Caicos Is'){ ?> selected <?php } ?> value="Turks & Caicos Is">Turks & Caicos Is</option>
       <option <?php if($u->country == 'Tuvalu'){ ?> selected <?php } ?> value="Tuvalu">Tuvalu</option>
       <option <?php if($u->country == 'Uganda'){ ?> selected <?php } ?> value="Uganda">Uganda</option>
       <option <?php if($u->country == 'United Kingdom'){ ?> selected <?php } ?> value="United Kingdom">United Kingdom</option>
       <option <?php if($u->country == 'Ukraine'){ ?> selected <?php } ?> value="Ukraine">Ukraine</option>
       <option <?php if($u->country == 'United Arab Emirates'){ ?> selected <?php } ?> value="United Arab Emirates">United Arab Emirates</option>
       <option <?php if($u->country == 'United States of America'){ ?> selected <?php } ?> value="United States of America">United States of America</option>
       <option <?php if($u->country == 'Uruguay'){ ?> selected <?php } ?> value="Uraguay">Uruguay</option>
       <option <?php if($u->country == 'Uzbekistan'){ ?> selected <?php } ?> value="Uzbekistan">Uzbekistan</option>
       <option <?php if($u->country == 'Vanuatu'){ ?> selected <?php } ?> value="Vanuatu">Vanuatu</option>
       <option <?php if($u->country == 'Vatican City State'){ ?> selected <?php } ?> value="Vatican City State">Vatican City State</option>
       <option <?php if($u->country == 'Venezuela'){ ?> selected <?php } ?> value="Venezuela">Venezuela</option>
       <option <?php if($u->country == 'Vietnam'){ ?> selected <?php } ?> value="Vietnam">Vietnam</option>
       <option <?php if($u->country == 'Virgin Islands (Brit)'){ ?> selected <?php } ?> value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
       <option <?php if($u->country == 'Virgin Islands (USA)'){ ?> selected <?php } ?> value="Virgin Islands (USA)">Virgin Islands (USA)</option>
       <option <?php if($u->country == 'Wake Island'){ ?> selected <?php } ?> value="Wake Island">Wake Island</option>
       <option <?php if($u->country == 'Wallis & Futana Is'){ ?> selected <?php } ?> value="Wallis & Futana Is">Wallis & Futana Is</option>
       <option <?php if($u->country == 'Yemen'){ ?> selected <?php } ?> value="Yemen">Yemen</option>
       <option  <?php if($u->country == 'Zaire'){ ?> selected <?php } ?> value="Zaire">Zaire</option>
       <option <?php if($u->country == 'Zambia'){ ?> selected <?php } ?> value="Zambia">Zambia</option>
       <option <?php if($u->country == 'Zimbabwe'){ ?> selected <?php } ?> value="Zimbabwe">Zimbabwe</option>
    </select>
    </div>
  </div>

</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>

      </div>
      </form>
    </div>
  </div>
</div>

<!-- edit user modal ends -->
                   
                              
                                <?php } } ?>
                           
                            </tbody>
                            <tfoot>
                              <tr>
                                   
                                   <th>Title</th>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Email</th>
                                    <th>Edit/Delete</th>
                                    

                                </tr>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- #/ container -->
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type="text/javascript">
  var $s=jQuery.noConflict();  
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "You want to delete this user" ,
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                type: "GET",
                url: "{{url('/delete_user')}}",
                data: {id:id},
                success: function (data) {

                    swal("Deleted!", "User has been Deleted.", "success");   
                    window.setTimeout(function() {
                            window.location.href ="{{url('/users')}}"
                        }, 1000);
                    }    
  
            });
    });
});

</script>



    



@endsection
