@extends('template.header')

@section('content')
<style>
.mainblk{min-height:163px;}
</style>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card gradient-1">
                        <a href="{{url('/artwork')}}" >
                            <div class="card-body mainblk">
                                <h3 class="card-title text-white">Intro Screens</h3>
                                <!-- <div class="d-inline-block">
                                    <h2 class="text-white">4565</h2>
                                    <p class="text-white mb-0">Jan - March 2019</p>
                                </div> -->
                                <span class="float-right display-5 opacity-5"><i class="fa fa-picture-o"></i></span>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card gradient-2">
                           <a href="{{url('/gallery')}}" ><div class="card-body mainblk">
                                <h3 class="card-title text-white">Canvas Management</h3>
                                <!-- <div class="d-inline-block">
                                    <h2 class="text-white">$ 8541</h2>
                                    <p class="text-white mb-0">Jan - March 2019</p>
                                </div> -->
                                <span class="float-right display-5 opacity-5"><i class="fa fa-picture-o"></i></span>
                            </div></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card gradient-3">
                        <a href="{{url('/stickers')}}" > <div class="card-body mainblk">
                                <h3 class="card-title text-white">Sticker Management</h3>
                                <!-- <div class="d-inline-block">
                                    <h2 class="text-white">4565</h2>
                                    <p class="text-white mb-0">Jan - March 2019</p>
                                </div> -->
                                <span class="float-right display-5 opacity-5"><i class="fa fa-picture-o"></i></span>
                            </div></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card gradient-4">
                        <a href="{{url('/timer')}}" >  <div class="card-body mainblk">
                              <h3 class="card-title text-white">Manage App time</h3>
                                <!-- <div class="d-inline-block">
                                    <h2 class="text-white">99%</h2>
                                    <p class="text-white mb-0">Jan - March 2019</p>
                                </div> -->
                                <span class="float-right display-5 opacity-5"><i class="fa fa-clock-o"></i></span>
                            </div></a>
                        </div>
                    </div>
                </div>














                    </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        @endsection
