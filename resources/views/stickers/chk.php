@extends('template.header')

@section('content')
<link rel="stylesheet" href="dist/css/lightbox.min.css">

<style type="text/css">

.img-block-wrap a.example-image-link img {
    margin-bottom: 16px;
}
.img-block-wrap {
    margin-bottom: 30px;
    text-align: center;
}
span.lb-caption {
    display: none !important;
}
.example-image-link{display:block;}
</style>


<div class="content-body">
<meta name="csrf-token" content="{{ csrf_token() }}">

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href >Art Works</a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
            <div class="card-body row">
            <div class="col-md-4">
                  <h4 class="card-title">Intro Screen Images(Looping Reel)</h4>
                  </div>

    <!-- <div class="col-md-4">
                        <div class="form-group filter-data">
    <form id="status-form" role="form" method="get" action="{{url('/intro_filter_status')}}">

                    <select onchange="this.form.submit()" class="form-control" name="status" id="status" >
                        <option @if(Request::get('status')=='') selected @endif value="" >Filter By Status</option>
                        <option @if(Request::get('status')=='1') selected @endif value="1">Active</option>
                        <option @if(Request::get('status')=='0') selected @endif value="0">InActive</option>
                        <option @if(Request::get('status')=='all') selected @endif value="all">All</option>

                    </select>
                    </form>
                </div>
                        </div>

                  <div class="col-md-4">
                  <button style="float:right;"type="button" class="btn mb-1 btn-primary" onclick="window.location.href='{{url('artwork_add')}}'">Add image</button>
                  </div> -->

                  </div>
                <div class="container">
<form class="form-valide" method="get" action="{{url('/deleteAll')}}" enctype="multipart/form-data">
@csrf


<input type="checkbox" id="checkAll">

<!-- <button class="btn btn-primary delete_all" type="button" name="bulk_delete" id="add_to_intro">Add To Intro Screen</button> -->



<a href="" data-toggle="modal" data-target="#basicExampleModal{{ $allcanva}}" class="btn btn-primary" data_value="{{ $allcanva}}" >Select Intro Screen Images</a>

<div class="modal fade" id="basicExampleModal{{ $allcanva}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      
        <h5 class="modal-title" id="exampleModalLabel">Select Images for Intro Screen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-validation">
  <form class="form-valide" method="post" action="{{url('/add_to_intro')}}" enctype="multipart/form-data">

                            @csrf
                            <div class="card">
            <div class="card-body row">
                <div class="col-md-4">
                <div class="row">
    <?php if(!empty($allcanva)){

         foreach ($allcanva as $dn) { ?>


<div class="col-md-3 col-sm-6 col-xs-12 img-block-wrap" id="{{$dn->c_id}}">

<input
type="checkbox" class="image_chk" id="check_all" name="img_ids[{{$dn->c_id}}]" data-id="{{$dn->c_id}}"
    <?php if ($dn->featured == 1) { ?>
    checked="checked"
    <?php } ?> 
    />


      <a class="example-image-link"
       href="{{asset('/storage/app/public')}}/{{$dn->image}}" data-lightbox="example-set"><img  style="border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 150px;height: 150px; max-width:100%;"
            src="{{asset('/storage/app/public')}}/{{$dn->image}}"
            class="example-image" src="{{asset('/storage/app/public')}}/{{$dn->image}}" alt=""/></a>
      
    </div>

          <?php } } ?>
          </div>
                
                
                    </div>
            </div></div>
</div>
      </div>
      <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary">Add</button>

      </div>
      </form>
    </div>
  </div>
</div>










<br>
<br>
<br>
<div class="row">
    <?php if(!empty($canvas_list)){

         foreach ($canvas_list as $dn) { ?>


<div class="col-md-3 col-sm-6 col-xs-12 img-block-wrap" id="{{$dn->c_id}}">

<input
type="checkbox" class="image_chk" id="check_all" name="img_ids[{{$dn->c_id}}]" data-id="{{$dn->c_id}}"
    <?php if ($dn->featured == 1) { ?>
    checked="checked"
    <?php } ?> 
    />


      <a class="example-image-link"
       href="{{asset('/storage/app/public')}}/{{$dn->image}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img  style="border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 150px;height: 150px; max-width:100%;"
            src="{{asset('/storage/app/public')}}/{{$dn->image}}"
            class="example-image" src="{{asset('/storage/app/public')}}/{{$dn->image}}" alt=""/></a>
       <!-- @if($dn->status==1)
    <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}">
    <button type="button" class="btn mb-1 btn-success st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Active</button>
    @else
    <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}">
    <button type="button" class="btn mb-1 btn-danger st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Inactive</button>

    @endif -->
    <a href="" class="btn btn-primary button" data-id="{{$dn->c_id}}">Remove</a>


    </div>

          <?php } } ?>
          </div>
          </div>
          </form>


  </div>
                </div>
            </div>
        </div>
    </div>
      </div>
      </div>

</div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
var $j=jQuery.noConflict();
$j('#checkAll').click(function () {
    if (this.checked) {
     $j('.image_chk').prop('checked', true);
    } else {
        $j('.image_chk').prop('checked', false);
    }
 });


    $j('#add_to_intro').on('click', function(e) {
            var allIds = [];
            $j(".image_chk:checked").each(function() {
                allIds.push($(this).attr('data-id'));
            });

            if(allIds.length <=0)
            {
                swal("Error!", "Please select atleast one intro screen", "error");

            }else{
            swal({
            title: "Are you sure!",
            text: "Do you want to add images to intro screen ?" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
                    var strIds = allIds.join(",");
                    //alert(strIds);
                    $.ajax({
                        url: "{{ url('/add_to_intro') }}",
                        type: 'POST',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+strIds,
                        success: function (data) {
                            if(data.success) {
                                swal("Added!", "Images has been added to intro screen.", "success");
                            }
                            swal("Added!", "Images has been added to intro screen.", "success");
                        },
                        error: function (data) {
                            //alert(data.responseText);
                        }
                    });
                });
            }
            if(allIds.length >3)
            {
                swal("Error!", "To add more intro screen images, please unselect one or more intro screen image", "error");

            }

    });

    


</script>


<script>

function change_status(val2){
    var $j=jQuery.noConflict();
          swal({
          title: "Are you sure?",
          text: "Do you really want to change status ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, change it!",
          closeOnConfirm: false
          },
          function(){
          var val=$j('#std_'+val2).val();
                    //  alert(val);
                    $.ajax({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    type: 'POST',
                    url: "{{url('/artwork_status_action')}}",
                    data: {status: val,id:val2},
                    success: function(result){
                     if(result==1)
                     {
                        $j('.st_'+val2).removeClass("btn-danger");

                        $j('.st_'+val2).addClass("btn-success");
                        $j('.st_'+val2).text('Active');
                        $j('#std_'+val2).val(result);
                     }
                     else{
                      $j('.st_'+val2).removeClass("btn-success");

                      $j('.st_'+val2).addClass("btn-danger");
                      $j('.st_'+val2).text('Inactive');

                      $j('#std_'+val2).val(result);

                     }
                    }
                    });
                    swal("Changed!", "Status has been Changed.", "success");

          });


          }

</script>

<script type="text/javascript">
  var $s=jQuery.noConflict();
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "You want to delete this canvas image" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                type: "GET",
                url: "{{url('/artwork_del')}}",
                data: {id:id},
                success: function (data) {

                    swal("Deleted!", "Canvas Image has been Deleted.", "success");
                    window.setTimeout(function() {
                            window.location.href ="{{url('/artwork')}}"
                        }, 2000);
                    }
                    

            });
    });
});

</script>


  <script src="dist/js/lightbox-plus-jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>



  @endsection
