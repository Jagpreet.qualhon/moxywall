@extends('template.header')

@section('content')
<link rel="stylesheet" href="dist/css/lightbox.min.css">

<style type="text/css">

.img-block-wrap a.example-image-link img {
    margin-bottom: 16px;
}
.img-block-wrap {
    margin-bottom: 30px;
    text-align: center;
}
span.lb-caption {
    display: none !important;
}
</style>


<div class="content-body">
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Canvas Background</a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
            <div class="card-body row">
            <div class="col-md-6">
                  <h4 class="card-title">Canvas List</h4>
                  </div>
                  <div class="col-md-6">
                  <button style="float:right;"type="button" class="btn mb-1 btn-primary" onclick="window.location.href='{{url('Add_image')}}'">Add image</button>
                  </div>

                  </div>
                <div class="container">



    <div class="row">
    <?php if(!empty($canvas_list)){

         foreach ($canvas_list as $dn) { ?>



<div class="col-md-3 col-sm-3 col-xs-12 img-block-wrap">
      <a class="example-image-link"
       href="{{asset('/storage/app/public')}}/{{$dn->image}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img  style="border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 150px;height: 150px;"
            src="{{asset('/storage/app/public')}}/{{$dn->image}}"
            class="example-image" src="{{asset('/storage/app/public')}}/{{$dn->image}}" alt=""/></a>

       @if($dn->status==1)
    <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}">
    <button type="button" class="btn mb-1 btn-success st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Active</button>
    @else
    <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->c_id}}">
    <button type="button" class="btn mb-1 btn-danger st_{{$dn->c_id}}" onclick="change_status(<?php echo $dn->c_id ?>)">Inactive</button>

    @endif
    <a href="" class="btn mb-1 btn-danger button" data-id="{{$dn->c_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>


    </div>
          <?php } } ?>

    </div>
  </div>
                </div>
            </div>
        </div>
    </div>
      </div>
      </div>

</div>
</div>
<script>

function change_status(val2){
    var $j=jQuery.noConflict();
          swal({
          title: "Are you sure?",
          text: "Do you really want to change status ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, change it!",
          closeOnConfirm: false
          },
          function(){
          var val=$j('#std_'+val2).val();
                    //  alert(val);
                    $.ajax({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    type: 'POST',
                    url: "{{url('/change_status')}}",
                    data: {status: val,id:val2},
                    success: function(result){
                     if(result==1)
                     {
                        $j('.st_'+val2).removeClass("btn-danger");

                        $j('.st_'+val2).addClass("btn-success");
                        $j('.st_'+val2).text('Active');
                        $j('#std_'+val2).val(result);
                     }
                     else{
                      $j('.st_'+val2).removeClass("btn-success");

                      $j('.st_'+val2).addClass("btn-danger");
                      $j('.st_'+val2).text('Inactive');

                      $j('#std_'+val2).val(result);

                     }
                    }
                    });
                    swal("Changed!", "Status has been Changed.", "success");

          });


          }

</script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
  var $s=jQuery.noConflict();
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "You want to delete this canvas image" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                type: "GET",
                url: "{{url('/delete_canvas')}}",
                data: {id:id},
                success: function (data) {

                    swal("Deleted!", "Canvas Image has been Deleted.", "success");
                    window.setTimeout(function() {
                            window.location.href ="{{url('/gallery')}}"
                        }, 4000);
                    }

            });
    });
});

</script>


  <script src="dist/js/lightbox-plus-jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>



  @endsection
