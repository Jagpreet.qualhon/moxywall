@extends('template.header')

@section('content')
<style>

div#DataTables_Table_0_filter input {
    border: 1px solid gray; border-radius:5px;
}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}
#img01 { width: 300px; /* Full width */
  height: 300px; /* Full height */}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 9999999; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
<div class="content-body">
<div class="box"><meta name="csrf-token" content="{{ csrf_token() }}">



<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                @if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                    <h4 class="card-title">Category List</h4>
                    <button style="float:right;"type="button" class="btn mb-1 btn-primary" onclick="window.location.href='{{url('category')}}'">Add Category</button>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                   
                                    <th>Category</th>
                                    <th>Status</th>
                                    <th>Edit/Delete Category</th>

                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($category)){ 
         
                         foreach ($category as $dn) { ?>
                            <tr>
                            <td>{{$dn->category_name}}</td>
                              <td> @if($dn->status==1)
                              <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->id}}"> 
                              <button type="button" class="btn mb-1 btn-success st_{{$dn->id}}" onclick="change_status(<?php echo $dn->id ?>)">Active</button>
                              @else
                              <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->id}}"> 
                              <button type="button" class="btn mb-1 btn-danger st_{{$dn->id}}" onclick="change_status(<?php echo $dn->id ?>)">Inactive</button>

                              @endif</td>
     <td>
     <a href="" data-toggle="modal" data-target="#basicExampleModal{{ $dn->id }}" class="btn mb-1 btn-danger" data_value="{{ $dn->id}}" ><i class="fa fa-edit" aria-hidden="true"></i></a>
     
     <a href="" class="btn mb-1 btn-danger button" data-id="{{$dn->id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
     </td>            
                                </tr>
                              
      
<div class="modal fade" id="basicExampleModal{{ $dn->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      <div class="modal-header">
      
        <h5 class="modal-title" id="exampleModalLabel">Update the {{ $dn->category_name }} Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" > 
      <div class="form-validation">
  <form class="form-valide" method="post" action="{{url('/update_cat')}}/{{$dn->id}}" enctype="multipart/form-data">

                            @csrf
<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="image_name">Category Name<span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
    <input class="form-control" name="cat_name" type="text" required value="{{$dn->category_name}}" placeholder="Add Category Name">
    </div>
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>

      </div>
      </form>
    </div>
  </div>
</div>




                                <?php } } ?>
                           
                            </tbody>
                            <tfoot>
                                <tr>
                             
                                
                                    <th>Category</th>
                                    <th>Status</th>
                                    <th>Edit/Delete Category</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->


















<!-- #/ container -->
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>



<script src="{{asset('assets/plugins/tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/tables/js/datatable-init/datatable-basic.min.js')}}"></script>
   
 

    <script src="dist/js/lightbox-plus-jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type="text/javascript">
 var $j=jQuery.noConflict();   
$j(document).ready(function () {
 
window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $j(this).remove(); 
    });
}, 5000);
 
});
</script>


<script>

function change_status(val2){
  var $j=jQuery.noConflict();   
swal({
title: "Are you sure?",
text: "Do you really want to change category status ?",
type: "warning",
showCancelButton: true,
confirmButtonClass: "btn-danger",
confirmButtonText: "Yes, change it!",
closeOnConfirm: false
},
function(){
var val=$j('#std_'+val2).val();
      //  alert(val);
      $.ajax({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      type: 'POST',
      url: "{{url('/change_status_cat')}}",
      data: {status: val,id:val2},
      success: function(result){
       if(result==1)
       {
          $j('.st_'+val2).removeClass("btn-danger");

          $j('.st_'+val2).addClass("btn-success");
          $j('.st_'+val2).text('Active');
          $j('#std_'+val2).val(result);
       }
       else{
        $j('.st_'+val2).removeClass("btn-success");

        $j('.st_'+val2).addClass("btn-danger");
        $j('.st_'+val2).text('Inactive');

        $j('#std_'+val2).val(result);

       }                
      }
      });
      swal("Changed!", "Category status has been Changed.", "success");

});         

    
}

</script>

 

    <script type="text/javascript">
  var $s=jQuery.noConflict();  
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "You want to delete this category" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                type: "GET",
                url: "{{url('/delete_category')}}",
                data: {id:id},
                success: function (data) {

                    swal("Deleted!", "Category has been Deleted.", "success");   
                    window.setTimeout(function() {
                            window.location.href ="{{url('/managecategory')}}"
                        }, 1000);
                    }    
  
            });
    });
});

</script>




  
@endsection
