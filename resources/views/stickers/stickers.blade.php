@extends('template.header')

@section('content')
<link rel="stylesheet" href="dist/css/lightbox.min.css">

<style type="text/css">


.img-block-wrap a.example-image-link img {
    margin-bottom: 16px;
    object-fit: contain;
}
.img-block-wrap {
    margin-bottom: 30px;
    text-align: center;
}
span.lb-caption {
    display: none !important;
}
.example-image-link{display:block;}
.img-block-wrap {
    display: inline-block;
}
.catlist {
    width: 100%;
    display: block;
}
.catlist h6 {
    margin: 20px;
    background: #bf028e;
    color: #fff;
    padding: 12px;
    font-weight: 600;
}
.btn {
    padding: 4px 16px;
}
.btn-success {
    color: #fff;
    background-color: #333;
    border-color: #333;
}

form#status-form {
    display: inline-block;
}

.form-group.filter-data.card-body {
    padding: 0px 22px;
}

a.btn.mb-1.editcat_stk.pull-right {
    position: relative;
    top: -54px;
    right: 32px;
     background-color: #333333;
    border: none;
    padding: 3px 10px; color:#fff;
}

</style>


<div class="content-body">
<meta name="csrf-token" content="{{ csrf_token() }}">

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif



<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Sticker Gallery</a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
            <div class="card-body row">
            <div class="col-md-4">
                  <h4 style="margin-left:-7px;" class="card-title">Stickers Gallery</h4>
                  </div>

        <div class="col-md-4">
                           
                            </div>


                  <div class="col-md-4">
                  <button style="float:right;"type="button" class="btn mb-1 btn-primary" onclick="window.location.href='{{url('add_sticker')}}'">Add Sticker</button>
                  </div>

                  </div>


                  <div class="form-group filter-data card-body">
        <form id="status-form" role="form" method="get" action="{{url('/filter_cat')}}">

        <label style="margin-top: 10px;margin-right: 7px;">Filter By Category</label>
      <select onchange="this.form.submit()" class="form-control"  required id="cat" name="cat" style="
    float: right;
    width: auto;
">
 
            <option value="all">All Stickers</option>
            @if(!empty($cat))
            @foreach($cat as $c)
            <option @if(Request::get('cat')==$c->id) selected @endif value="{{$c->id}}">{{$c->category_name}}</option>
            @endforeach
            @endif

            </select>



                        </form>
                    </div>
                <div class="container">



    <div class="row">
@if(count($stickers)==0)
<h6 class="card-body">No stickers found under this category !!  <a style="color:#2e28f6;" href="{{url('/stickers')}}">Click Here For All Stickers</a></h6>
@endif

<?php if(!empty($data)){  
 
foreach ($data as $cat) { 
    if(!empty($cat[0]->cat_name)){
    echo '<div class="catlist"><h6>'.$cat[0]->cat_name.'</h6>'; ?>
<a href="" data-toggle="modal" data-target="#basicExampleModal{{$cat[0]->cat_id }}" class="btn mb-1 editcat_stk pull-right" data_value="{{ $cat[0]->cat_id}}" >Edit</a>
    
<div class="modal fade" id="basicExampleModal{{ $cat[0]->cat_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      
        <h5 class="modal-title" id="exampleModalLabel">Update the {{ $cat[0]->cat_name }} Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-validation">
  <form class="form-valide" method="post" action="{{url('/update_cat')}}/{{$cat[0]->cat_id}}" enctype="multipart/form-data">

                            @csrf
<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="image_name">Category Name<span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
    <input class="form-control" required name="cat_name" type="text" value="{{$cat[0]->cat_name}}" placeholder="Update Category Name">
    </div>
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>

      </div>
      </form>
    </div>
  </div>
</div>



<?php }
//print_r($cat);
        foreach($cat as $dn){ ?>
          
<div class="col-md-2 col-sm-6 col-xs-12 img-block-wrap">
     <a class="example-image-link"
      href="{{asset('/storage/app/public')}}/{{$dn->image}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
      <img  style="border: 1px solid #ddd;
           border-radius: 4px;
           padding: 5px;
           width: 150px;height: 150px;"
           src="{{asset('/storage/app/public')}}/{{$dn->image}}"
           class="example-image" src="{{asset('/storage/app/public')}}/{{$dn->image}}" alt=""/></a>

      @if($dn->status==1)
   <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->st_id}}">
   <button type="button" class="btn mb-1 btn-success st_{{$dn->st_id}}" onclick="change_status_stk(<?php echo $dn->st_id ?>)">Active</button>
   @else
   <input type="hidden" value="{{$dn->status}}" id="std_{{$dn->st_id}}">
   <button type="button" class="btn mb-1 btn-danger st_{{$dn->st_id}}" onclick="change_status_stk(<?php echo $dn->st_id ?>)">Inactive</button>

   @endif
   
   <a href="" class="btn mb-1 btn-danger button" data-id="{{$dn->st_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>


   </div>
         <?php } ?>
         </div>
         
        
        
   <?php     }
    }?>



<script>

function change_status_stk(val2){
    var $j=jQuery.noConflict();
          swal({
          title: "Are you sure?",
          text: "Do you really want to change sticker's status?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, change it!",
          closeOnConfirm: false
          },
          function(){
          var val=$j('#std_'+val2).val();
                    //  alert(val);
                    $.ajax({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    type: 'POST',
                    url: "{{url('/change_status_stk')}}",
                    data: {status: val,id:val2},
                    success: function(result){
                     if(result==1)
                     {
                        $j('.st_'+val2).removeClass("btn-danger");

                        $j('.st_'+val2).addClass("btn-success");
                        $j('.st_'+val2).text('Active');
                        $j('#std_'+val2).val(result);
                     }
                     else{
                      $j('.st_'+val2).removeClass("btn-success");

                      $j('.st_'+val2).addClass("btn-danger");
                      $j('.st_'+val2).text('Inactive');

                      $j('#std_'+val2).val(result);

                     }
                    }
                    });
                    swal("Changed!", "Sticker's status has been changed successfully.", "success");

          });


          }

</script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
  var $s=jQuery.noConflict();
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "You want to delete this sticker image" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                type: "GET",
                url: "{{url('/delete_sticker')}}",
                data: {id:id},
                success: function (data) {

                    swal("Deleted!", "Sticker Image has been Deleted.", "success");
                    window.setTimeout(function() {
                            window.location.href ="{{url('/stickers')}}"
                        }, 1000);
                    }

            });
    });
});

</script>


  <script src="dist/js/lightbox-plus-jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>



  @endsection
