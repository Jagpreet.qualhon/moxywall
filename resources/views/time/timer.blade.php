@extends('template.header')

@section('content')

<style>
.dispddate {
    display: block;
    text-align:center
}
canvas {
    display: block;
    width: 30%;
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
}
form.form-valide .form-group {
    text-align: center;
}
form.form-valide .col-form-label {
    display: inline-block;
}
form.form-valide .timeclsfield {
    display: inline-block;
}
.timersubbtn {
    margin: 0 auto;
    padding-bottom: 30px;
}

</style>

<div class="content-body">

<meta name="csrf-token" content="{{ csrf_token() }}">
@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




            <div class="card">
                <div class="card-body">
                <h5 class="timercls">Set Access Timings For App</h5>
<div class="form-validation">
    <form class="form-valide" method="post" action="{{url('/add_time')}}" enctype="multipart/form-data">
                 @csrf
<div class="form-group">
    <label class="col-lg-2 col-xs-12 col-form-label" for="image_name">Set start time <span class="text-danger">*</span>
    </label>
    <div class="col-lg-4 timeclsfield">
    <input class="form-control start_time" required name="from_dt" type="time" id="date-time from_dt" value="{{ $timedata[0]->from_dt}}" placeholder="Select Start DateTime">
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 col-xs-12 col-form-label" for="image_name">Set end Time <span class="text-danger">*</span>
    </label>
    <div class="col-lg-4 timeclsfield">
      
        <input type="time" class="form-control end_time" id="date-time2 to_dt"  value="{{ $timedata[0]->to_dt}}"required name="to_dt" placeholder="Select End DateTime">
    </div>
</div>



</div>
      </div>
<div class="timersubbtn">
        <button type="submit" class="btn btn-primary savebtn" id="savebtn">Save changes</button>
</div>
     
      </form>
    </div>

    </div>




                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->
</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 <script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>

<script>
  var $s=jQuery.noConflict();
 $s('#savebtn').click(function(e) {
    var st = $s('input[name="from_dt"]').val();
    var end =$s('input[name="to_dt"]').val();
    if(end < st){
        e.preventDefault();
        swal({
            title: "Entered end time is less than start time !",
            text: "Please enter end time greater than start time !!" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "OK!",
            showCancelButton: false,
        });

    }
   
});

</script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type="text/javascript">
  var $s=jQuery.noConflict();

$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "Do you really want to clear the DateTime Timer ??" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{url('/clear_timer')}}",
                data: {id:id},
                success: function (data) {

                    swal("Cleared!", "DateTime Timer has been cleared successfully !!", "success");
                    window.setTimeout(function() {
                            window.location.href ="{{url('/timescreen')}}"
                        }, 2000);
                    }

            });
    });
});

</script>

@endsection
