@extends('template.header')

@section('content')

<div class="content-body">

<meta name="csrf-token" content="{{ csrf_token() }}">
@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>	

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>	

        <strong>{{ $message }}</strong>

</div>

@endif
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($timedata[0]->status==1)
<div class="card">
<div class="card-body">
<form class="form-valide" method="POST" action="{{url('/clear_timer')}}" enctype="multipart/form-data">
    @csrf
<button type="submit" data-id="{{$timedata[0]->id}}" class="btn btn-primary button">Clear DateTimer</button>
    
</form>
</div>


</div>
@endif
            <div class="card">
                <div class="card-body">
                
                    <div class="form-validation">
                        <form class="form-valide" method="post" action="{{url('/add_time')}}" enctype="multipart/form-data">
                       
                            @csrf
<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="image_name">Start DateTime <span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
    <input class="form-control" name="from_dt" type="text" id="date-time" value="{{ $timedata[0]->from_dt}}" placeholder="Select Start DateTime...">
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-4 col-form-label" for="image_name">End DateTime <span class="text-danger">*</span>
    </label>
    <div class="col-lg-6">
        <input type="text" class="form-control" id="date-time2"  value="{{ $timedata[0]->to_dt}}"required name="to_dt" placeholder="Select End DateTime..">
    </div>
</div>
                          
                           
                                                 
                        
                          
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->
</div>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 <script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>


<script>
  var $j=jQuery.noConflict();    
  $j(function () {
        $j('#date-time, #date-time2').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
    });
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type="text/javascript">
  var $s=jQuery.noConflict();  
$s(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $s(this).data('id');
    swal({
            title: "Are you sure!",
            text: "Do you really want to clear the DateTime Timer ??" ,
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{url('/clear_timer')}}",
                data: {id:id},
                success: function (data) {

                    swal("Cleared!", "DateTime Timer has been cleared successfully !!", "success");   
                    window.setTimeout(function() {
                            window.location.href ="{{url('/timescreen')}}"
                        }, 4000);
                    }    
  
            });
    });
});

</script>


@endsection
