@extends('template.header')

@section('content')

<style>
.my-select {
  display: none;
}
</style>

<div class="content-body">

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">

        @if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>	

        <strong>{{ $message }}</strong>

</div>

@endif

@if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>	

        <strong>{{ $message }}</strong>

</div>

@endif


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="card">
                <div class="card-body">
                <div class="card">
                <?php 
                $vdo = DB::table('tutorial_video')->get();
                if(count($vdo)==0){   ?>
                    <h4 class="card-title">Add Tutorial Video</h4>
                <?php }else{ ?>
                    <h4 class="card-title">Change Tutorial Video</h4>
                    <p>You can upload tutorial video here. This will replace the current tutorial video which you had uploaded previously !!</p>
                <?php } ?>
                        </div>
                    <div class="form-validation">
                        <form class="form-valide" method="post" action="{{url('/upload_video')}}" enctype="multipart/form-data">
                       
        @csrf
       
        <div class="form-group row">
            <label class="col-lg-4 col-form-label" for="myfile">Upload Video<span class="text-danger">*</span>
            </label>

            <div class="col-lg-6">
                <input id="video" name="video"  type="file" required class="" >
            </div>
        </div>        
                        
                          
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->
</div>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
 ></script>
 <script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>

@endsection
