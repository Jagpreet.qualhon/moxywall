<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/canvas', 'ApiController@index');
Route::get('/loop_reel', 'ApiController@loop_reel');
Route::get('/stickers', 'ApiController@stickers');
Route::post('/user', 'ApiController@user');
Route::get('/country', 'ApiController@country');
Route::get('/video', 'ApiController@tutorial_video');
Route::post('/new_artwork', 'ApiController@new_artwork');
Route::get('/access_time', 'ApiController@app_access_time');