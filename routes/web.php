<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth/login');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index')->middleware('auth');

Route::get('admin/home', 'AdminController@index')->middleware('auth');
Route::get('canvas_list', 'AdminController@canvas_list')->middleware('auth');
Route::get('Add_image', 'AdminController@Add_image')->middleware('auth');
Route::post('add_canvas_image', 'AdminController@add_canvas_image')->middleware('auth');

Route::post('change_status', 'AdminController@change_status')->middleware('auth');
Route::get('delete_canvas','AdminController@delete_canvas');
Route::get('deleteAll', 'AdminController@deleteAll');


Route::get('artwork', 'ArtworkController@index')->middleware('auth');
Route::get('artwork_add', 'ArtworkController@create')->middleware('auth');
Route::post('artwork_add_action', 'ArtworkController@store')->middleware('auth');
Route::post('artwork_status_action', 'ArtworkController@update')->middleware('auth');
Route::get('artwork_delete','ArtworkController@destroy');
Route::get('artwork_del','ArtworkController@delete');
Route::get('artwork_filter_status','ArtworkController@filter_status')->middleware('auth');
Route::get('intro_filter_status','ArtworkController@filter_status1')->middleware('auth');
Route::get('introscreen','ArtworkController@introscreen')->middleware('auth');
Route::post('add_to_intro','ArtworkController@add_to_intro')->middleware('auth');
Route::post('remove_from_intro','ArtworkController@remove_from_intro')->middleware('auth');

Route::get('chk', 'ArtworkController@chk')->middleware('auth');
Route::post('remove_feat', 'ArtworkController@remove_feat')->middleware('auth');


Route::get('gallery', 'AdminController@gallery')->middleware('auth');

Route::get('timescreen', 'AdminController@timescreen')->middleware('auth');
Route::get('timer', 'AdminController@timer')->middleware('auth');
Route::post('add_time', 'AdminController@add_time')->middleware('auth');
Route::post('clear_timer', 'AdminController@clear_timer')->middleware('auth');

Route::get('category', 'AdminController@add_cat')->middleware('auth');
Route::post('addcat', 'AdminController@addcat')->middleware('auth');

Route::get('add_sticker', 'AdminController@add_sticker')->middleware('auth');
Route::post('addsticker', 'AdminController@addsticker')->middleware('auth');
Route::get('stickers', 'AdminController@stickers')->middleware('auth');
Route::post('change_status_stk', 'AdminController@change_status_stk')->middleware('auth');
Route::get('delete_sticker','AdminController@delete_sticker');
Route::get('managecategory','AdminController@managecategory');
Route::post('change_status_cat', 'AdminController@change_status_cat')->middleware('auth');
Route::get('delete_category','AdminController@delete_category')->middleware('auth');
Route::get('filter_cat','AdminController@filter_cat')->middleware('auth');
Route::get('video','AdminController@add_video')->middleware('auth');
Route::post('upload_video','AdminController@upload_video')->middleware('auth');


Route::post('update_cat/{id}','AdminController@update_cat')->middleware('auth');

Route::get('change_pwd','AdminController@change_pwd')->middleware('auth');
Route::post('update_pwd','AdminController@update_pwd')->middleware('auth');
Route::get('filter_status','AdminController@filter_status')->middleware('auth');

Route::get('users','FrontEndUserController@index')->middleware('auth');
Route::post('update_user/{id}','FrontEndUserController@update_user')->middleware('auth');
Route::get('delete_user','FrontEndUserController@delete_user')->middleware('auth');


Route::get('/mail', 'ApiController@testmail');
